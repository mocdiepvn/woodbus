# export DJANGO_SETTINGS_MODULE=woodbus.dev_settings
# python manage.py behave
# coverage run --source=woodbus/ manage.py behave
# coverage report -m --skip-empty --skip-covered
# coverage html && browse htmlcov/index.html
# splinter: readthedoc still has python 2.7 only
# splinter: installation doc does not mention to install geckodriver for firefox
# For woking: put @wip to feature file
# Then add --tags=wip or -w to behave command

# [Django: How to manage development and production settings?]: https://stackoverflow.com/questions/10664244/django-how-to-manage-development-and-production-settings
# [Django Settings for Production and Development: Best Practices]: https://dzone.com/articles/django-settings-production-and
# [Django Tutorial Part 11: Deploying Django to production]: https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Deployment

# [behave/behave-django] is supported by people supporting behave/behave
# [behave/behave-django]: https://github.com/behave/behave-django
# [behave-django]: https://behave-django.readthedocs.io/en/stable/
# [django-behave/django-behave] has last updated date on Apr 29, 2018
# [django-behave/django-behave]: https://github.com/django-behave/django-behave/issues

# [Coverage.py]: https://coverage.readthedocs.io/en/coverage-5.0.3/#quick-start
# [Coverage.py for Django templates]: https://nedbatchelder.com/blog/201501/coveragepy_for_django_templates.html

# [Behavior Driven Development]: https://behave.readthedocs.io/en/stable/philosophy.html
# [Introducing BDD]: https://dannorth.net/introducing-bdd/
# [What’s in a Story?]: https://dannorth.net/whats-in-a-story/
# [Business value]: https://en.wikipedia.org/wiki/Business_value
# [Search Results: value stream]: https://www.lean.org/search/?sc=value+stream
# [You aren't gonna need it]: https://en.wikipedia.org/wiki/You_aren%27t_gonna_need_it

# [geckodriver]: https://github.com/mozilla/geckodriver/releases/tag/v0.26.0
# pushd /tmp
# wget https://github.com/mozilla/geckodriver/releases/download/v0.26.0/geckodriver-v0.26.0-linux64.tar.gz
# tar -xvzf geckodriver-v*.tar.gz
# popd
# mv /tmp/geckodriver env/bin/

# This is safe due to native of settings files is only constants
from woodbus.settings import *

INSTALLED_APPS += [
        'behave_django',
        ]

#mkdir -p features/steps
# For preparing behave environment
#echo "##from myapp.main.tests.factories import UserFactory, RandomContentFactory
###from rest_framework.test import APIClient
#
#
##def django_ready(context):
##    # This function is run inside the transaction
##    #UserFactory(username='user1')
##    #UserFactory(username='user2')
##    #RandomContentFactory()
##    # Modify the test instance
##    #context.test.client = APIClient()
##    context.django = True
#
#
##def before_all(context):
##    context.fixtures = ['user-data.json']
#
#
##def before_scenario(context, scenario):
##    if scenario.name == 'User login with valid credentials':
##        context.fixtures = ['user-data.json']
##    elif scenario.name == 'Check out cart':
##        context.fixtures = ['user-data.json', 'store.json', 'cart.json']
##    else:
##        # Resetting fixtures, otherwise previously set fixtures carry
##        # over to subsequent scenarios.
##        context.fixtures = []
#
#
##def before_feature(context, feature):
##    if feature.name == 'Login':
##        context.fixtures = ['user-data.json']
##        # This works because behave will use the same context for
##        # everything below Feature. (Scenarios, Outlines, Backgrounds)
##    else:
##        # Resetting fixtures, otherwise previously set fixtures carry
##        # over to subsequent features.
##        context.fixtures = []" > features/environment.py

# For creating new feature
# source .bash_pybh_aliases
# new_feature <feature_name>

# To use short command
# coverage run && coverage report -m
#echo "# .coveragerc to control coverage.py
#[run]
#branch = True
#command_line = manage.py behave
##source =
#omit =
#    */wsgi.py
#    manage.py
#plugins =
#    django_coverage_plugin
#
#[html]
#directory = htmlcov" > .coveragerc
