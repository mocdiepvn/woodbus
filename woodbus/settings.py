"""
Django settings for woodbus project.

Generated by 'django-admin startproject' using Django 2.2.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os

from django.utils.translation import gettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '&s&*c#0=d%4t_6^nh9g2-9%alre=11556qhlm)_43dc_gbqu-$'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'cities_light',
    'main.apps.MainConfig',
    'request_token',
    'hash_email_auth.apps.HashEmailAuthConfig',
    'bus_time.apps.BusTimeConfig',
]

AUTH_USER_MODEL = 'hash_email_auth.User'
EMAIL_HOST = 'mail.mocdiep.com'
EMAIL_PORT = 465
EMAIL_HOST_USER = 'test_poster@mocdiep.com'
EMAIL_HOST_PASSWORD = 'P0CW21(iRB=1'
# Long time to process
#EMAIL_USE_TLS = True
EMAIL_USE_SSL = True
HOME_URL_NAME = 'bus_time:index'
SEARCH_INPUT_URL_NAME = 'bus_time:search_input'
AUTH_STATUS_URL_NAME = 'hash_email_auth:status'

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'request_token.middleware.RequestTokenMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'bus_time.middlewares.TimezoneMiddleware',
]

ROOT_URLCONF = 'woodbus.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'woodbus.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGES = [
    ('en', _('English')),
    ('vi', _('Vietnamese')),
]

LANGUAGES_FLAGS = {
    'en': 'gb',
}

#LANGUAGE_CODE = 'en-us'
LANGUAGE_CODE = 'en'

# should not use other timezone than UTC util logic is fixed
# TIME_ZONE = 'Asia/Ho_Chi_Minh'
TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

CITIES_LIGHT_TRANSLATION_LANGUAGES = ['vi', 'en']
CITIES_LIGHT_INCLUDE_COUNTRIES = ['VN']
CITIES_LIGHT_INCLUDE_CITY_TYPES = ['PPL', 'PPLA', 'PPLA2', 'PPLA3', 'PPLA4', 'PPLC', 'PPLF', 'PPLG', 'PPLL', 'PPLR', 'PPLS', 'STLMT',]
# Skip translation file
CITIES_LIGHT_TRANSLATION_SOURCES = []

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'
