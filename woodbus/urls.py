"""woodbus URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.urls import include, path

from main.views import (ContributorView, TermsAndCondsView, TranslationView,
        WIPPageView)


urlpatterns = i18n_patterns(
    path('admin/', admin.site.urls),
    path('auth/', include('hash_email_auth.urls')),
    path('contributors/', ContributorView.as_view(), name='contributors'),
    path('translations/', TranslationView.as_view(), name='translations'),
    path('terms_and_conds/', TermsAndCondsView.as_view(),
            name='terms_and_conds'),
    path('wip/', WIPPageView.as_view(), name='wip'),
    path('', include('bus_time.urls')),
    prefix_default_language=False,
)
