from django.conf import settings
from django.contrib.auth import logout
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, render_to_response, reverse
from django.utils.translation import gettext as _
from django.views.generic import View
from django.views.generic.edit import FormView
from request_token.decorators import use_request_token
from request_token.models import RequestToken

from .forms import EmailAuthenticationForm
from .models import User, TOKEN_SCOPE

# hash_email_auth/templates/hash_email_auth/login.html
login_template = 'hash_email_auth/login.html'


def get_home_url():
    url = reverse(settings.HOME_URL_NAME)
    return url


def status(request, *args, **kwargs):
    #print('status:', args, kwargs, sep='\n')
    if request.user.username:
        context = {
                'login_dropdown': request.user.username,
                'logged_in_user': request.user,
                }
    else:
        context = {
                'login_dropdown': _('Login'),
                }
    #print(context)
    return render(request, login_template, context)


@use_request_token(scope=TOKEN_SCOPE)
def token_login(request, *args, **kwargs):
    #print('login:', args, kwargs, sep='\n')
    #print(request.GET, request.POST, sep='\n')
    #print(request.token, request.user)
    if request.user.is_active: pass
    else:
        request.user.activate()
    context = {}
    #return render(request, login_template, context)
    #print('token_login')
    return HttpResponseRedirect(get_home_url(), status=307)


class LoginView(FormView):
    template_name = login_template
    form_class = EmailAuthenticationForm

    def form_valid(self, form):
        server_host = self.request.get_host()
        user = User.objects.pre_authenticate(form.cleaned_data['login_email'])
        token = user.get_login_token()
        form.send_email(server_host, token)
        context = {
                'sent_email': True,
                }
        #print('form_valid', context)
        return self.render_to_response(context)

    def form_invalid(self, form):
        context = {
                'incorrect_email': True,
                }
        # /home/hailang/Django/env_cities/lib/python3.6/site-packages/django/views/generic/base.py|137|  TypeError: __init__() got an unexpected keyword argument 'status_code'
        #print('form_invalid', context)
        return self.render_to_response(context, status=400)


class LogoutView(View):
    def get(self, request, *args, **kwargs):
        logout(request)
        response = HttpResponse()
        response['X-IC-Redirect'] = get_home_url()
        return response
