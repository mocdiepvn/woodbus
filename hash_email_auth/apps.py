from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class HashEmailAuthConfig(AppConfig):
    name = 'hash_email_auth'
    verbose_name = _('Authentication using hashed email')
