from django.urls import include, path


from . import views as auth_views


app_name = 'hash_email_auth'

urlpatterns = [
    path('', auth_views.status, name='status'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('token_login/', auth_views.token_login, name='token_login'),
]
