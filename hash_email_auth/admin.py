from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import User
from request_token import models as rtmdl


class RequestTokenInline(admin.TabularInline):
    model = rtmdl.RequestToken
    fields = (
        'scope',
        'not_before_time',
        'expiration_time',
        'max_uses',
        'used_to_date',
        'issued_at',
    )
    readonly_fields = ('scope', 'issued_at')


class UserAdmin(admin.ModelAdmin):
    inlines = [
            RequestTokenInline,
            ]


admin.site.register(User, UserAdmin)
