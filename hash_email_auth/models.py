import hashlib

from django.db import models
#from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import (
    UserManager, AbstractUser
)
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from request_token.models import RequestToken

TOKEN_SCOPE = 'hash_email_auth:token_login'


class HashedEmailBackend:
    ''' Authenticate user using email and token. '''


# https://docs.djangoproject.com/en/2.2/topics/auth/customizing/
class HashedEmailUserManager(UserManager):
    def create_user(self, username=None, email=None, hashed_email=None,
            password=None, active=True):
        ''' Creates and saves a User with the given email in hashed form.  '''
        hashed_email = hashed_email or (self.hash_email(email) if email
                else None)
        #print('password', password, email, hashed_email)
        if email and password:
            user = self.model(username=username, password=password,
                    email=email, hashed_email=hashed_email)
        elif email:
            hashed_email = hashed_email or self.hash_email(email)
            user = self.model(username=username, hashed_email=hashed_email)
        else:
            raise ValueError(_('Users must have a valid email address.'))

        user.is_active = active
        user.save(using=self._db)
        return user

    def hash_email(self, email):
        ''' Hash email before saving to prevent viewing it. '''
        hashed_email = 'sha3_512.' + hashlib.sha3_512(bytes(email, 'utf-8')
                ).hexdigest()
        return hashed_email

    def get(self, email=None, *args, **kwargs):
        if email:
            kwargs['hashed_email'] = self.hash_email(email)
        else: pass
        return super().get(*args, **kwargs)

    def pre_authenticate(self, email):
        max_id = self.all().aggregate(models.Max('pk'))['pk__max'] or 0
        username = 'user_' + str(max_id + 1)
        try:
            hashed_email = self.hash_email(email)
            user = self.get(hashed_email=hashed_email)
        except (self.model.DoesNotExist):
            user = self.create_user(username=username, email=email,
                    hashed_email=hashed_email, active=False)
        try:
            token = user.get_login_token()
        except (RequestToken.DoesNotExist):
            token = user.request_tokens.create_token(scope=TOKEN_SCOPE,
                    user=user, login_mode=RequestToken.LOGIN_MODE_SESSION)
        #print('pre_authenticate', token.expiration_time)
        return user


class User(AbstractUser):
    hashed_email = models.CharField(_('Hashed email'), unique=True,
            max_length=256, blank=True, null=True, editable=False,
            db_index=True)
    email = models.EmailField(_('Email address'), blank=True, null=True)

    objects = HashedEmailUserManager()

    def activate(self):
        self.is_active = True
        self.save()

    def get_login_token(self):
        return self.request_tokens.get(scope=TOKEN_SCOPE,
                login_mode=RequestToken.LOGIN_MODE_SESSION, used_to_date=0,
                expiration_time__gt=timezone.now())
