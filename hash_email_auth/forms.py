from django import forms
from django.conf import settings
from django.core.mail import send_mail, EmailMultiAlternatives
from django.shortcuts import reverse
from django.template.loader import render_to_string
#from django.utils.translation import gettext as _

from hash_email_auth.models import TOKEN_SCOPE

# hash_email_auth/templates/hash_email_auth/email_message.txt
# hash_email_auth/templates/hash_email_auth/email_message.html
EMAIL_TEMPLATE = 'hash_email_auth/email_message'


class EmailAuthenticationForm(forms.Form):
    login_email = forms.EmailField()

    error_messages = {
            }

    def send_email(self, server_host, request_token, is_testing=False):
        email = self.cleaned_data['login_email']
        #subject = _('Login to woodbus')
        subject = 'Login to woodbus'
        #print('send_email', server_host)
        login_url = reverse(TOKEN_SCOPE) + '?rt=' + request_token.jwt()
        context = {
                'server_host': 'https://' + server_host,
                'login_url': login_url,
                'expiration_time': request_token.expiration_time,
                }
        #print('context', context, 'login_url', login_url)
        message = render_to_string(EMAIL_TEMPLATE + '.txt', context)
        message_html = render_to_string(EMAIL_TEMPLATE + '.html', context)
        emsg = EmailMultiAlternatives(subject, message,
                from_email=settings.EMAIL_HOST_USER, to=(email,))
        emsg.attach_alternative(message_html, "text/html")
        if is_testing:
            return emsg

        result = emsg.send()
        #print('result:', result) # 1
        return result
