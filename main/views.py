from django.conf import settings
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.translation import get_language
from django.views.generic import TemplateView

from hash_email_auth.models import User


auto_load_tmpl = ' ic-get-from="{0}" ic-trigger-on="load"'


def get_home_url():
    return settings.HOME_URL_NAME


def get_available_languages(active_code=None):
    LANGUAGES_FLAGS = settings.LANGUAGES_FLAGS
    LANGUAGES = settings.LANGUAGES
    active_code = active_code or get_language()
    #print('"' + str(LANGUAGES_FLAGS) + '"', active_code)
    result = [(active_code, LANGUAGES_FLAGS.get(active_code, active_code))]
    result.extend([(code, LANGUAGES_FLAGS.get(code, code))
            for code, name in LANGUAGES if code != active_code])
    #print(result)
    return result


def index(request, auto_load=''):
    #print('get_url')
    search_text = request.GET.get('q') or ''
    if search_text:
        search_text = '?q=' + search_text
        auto_load = auto_load_tmpl.format(
                reverse('bus_time:search') + search_text)
    else: pass
    context = {
            'search_text': search_text, # for search_input
            'auto_load': auto_load,
            'home_url_name': get_home_url(),
            'search_input_url_name': settings.SEARCH_INPUT_URL_NAME,
            'auth_status_url_name': settings.AUTH_STATUS_URL_NAME,
            'LANGUAGES': get_available_languages(),
            }
    #print(context)
    # main/templates/main/index.html
    return render(request, 'main/index.html', context)


class WIPPageView(TemplateView):
    # main/templates/main/wip.html
    template_name = 'main/wip.html'


class ContributorView(TemplateView):
    # main/templates/main/contributors.html
    template_name = 'main/contributors.html'

    def get_context_data(self):
        return { 'home_url_name': get_home_url() }


class TranslationView(TemplateView):
    # main/templates/main/translations.html
    template_name = 'main/translations.html'


class TermsAndCondsView(TemplateView):
    # main/templates/main/terms_and_conds.html
    template_name = 'main/terms_and_conds.html'

    def get_context_data(self):
        context = super().get_context_data()
        request = self.request
        if request.session.test_cookie_worked():
            #print('delete_test_cookie')
            request.session.delete_test_cookie()
        elif request.session.__dict__['_SessionBase__session_key']: pass
        else:
            # https://stackoverflow.com/questions/5595166/how-to-update-a-cookie-in-django
            # Still failed
            #response = redirect('wip')
            #response.set_cookie('csrftoken', 'test_csrftoken')
            #request.session.delete_test_cookie()
            # KeyError: 'testcookie'
            request.session.set_test_cookie()
        cookie = request.COOKIES
        session = request.session.__dict__.copy()
        # Still no cookie in render_to_response
        #print('TermsAndCondsView', cookie, session)
        #print('context', context)
        # These are not user's information so user doesn't need to see them
        for key in ['_session_cache', 'model', 'serializer']:
            try:
                session.pop(key)
            except KeyError: pass
        if 'csrftoken' in cookie:
            cookie['csrftoken'] = ('https://docs.djangoproject.com/en/2.2/'
                    + 'topics/security/#cross-site-request-forgery-'
                    + 'csrf-protection')
        else: pass
        hashed_email = User.objects.hash_email('user@company.com')
        context.update({
                'home_url_name': get_home_url(),
                'cookie': cookie,
                'session': session,
                'hashed_email': hashed_email.split('.')[1],
                })
        return context
