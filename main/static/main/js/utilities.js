//print = console.log;
var exclude_elems = [];
var in_show_hide = '';

function show_hide(elem_id, contain_elems=[]) {
    //print('start show_hide')
    in_show_hide = elem_id;
    openDropdown = document.getElementById(elem_id);
    openDropdown.classList.toggle("w3-show");
    if (openDropdown.classList.contains('w3-show')) {
        exclude_elems = contain_elems;
    } else {
        exclude_elems = [];
    }
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
    //print('onclick', in_show_hide, event.target.matches('#show_hide_' + in_show_hide))
    //print(event)
    //print('onclick', exclude_elems);
    for (i = 0; i < exclude_elems.length; i++) {
        if (event.target.matches('#' + exclude_elems[i])) {
            return;
        }
    }
    var dropdowns = document.getElementsByClassName("w3-dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (in_show_hide != '' && openDropdown.matches('#' + in_show_hide)) {
            continue
        }
        if (openDropdown.classList.contains('w3-show')) {
            openDropdown.classList.remove('w3-show');
        }
    }
    in_show_hide = ''
}
