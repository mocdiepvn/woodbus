from django.test import TestCase, Client


def get_client(context, device):
    #print('Device:', device)
    try:
        client = context.devices[device]
    except (AttributeError):
        client = context.test.client
        context.devices = {device: client}
    except (KeyError):
        client = Client()
        context.devices[device] = client
    #print(context.devices)
    return client
