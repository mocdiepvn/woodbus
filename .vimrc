nnoremap <F4> :set makeprg=./djbh.sh\ --wip<CR>:make!<CR>:copen<CR>G10k
nnoremap <leader><F4> :set makeprg=./djbh.sh\ --format\ plain<CR>:make!<CR>:copen<CR>G10k
" For BDD (w, s, v: feature at top, a, d, g: feature at left)
nnoremap <leader>w ^f'l"zyi'q/i<C-R>z<ESC>:s/\v"[^"]+"/".\\+"/g<CR><CR><C-W>kggn
nnoremap <leader>W ^f'l"zyi'/<C-R>z<CR><C-W>kggn
"nnoremap <leader>W ^f'l"zyi'q/i<C-R>z<ESC>0f"lci".\+<CR><C-W>kggn
nnoremap <leader>s ^W"zy$q/"zp:s/\v"[^"]+"/".\\+"/g<CR><CR><C-W>jggn
nnoremap <leader>S ^W"zy$/<C-R>z<CR><C-W>jggn
"nnoremap <leader>S ^W"zy$q/"zp0f"lci".\+<CR><C-W>jggn
nnoremap <leader>a ^f'l"zyi'q/i<C-R>z<ESC>:s/\v"[^"]+"/".\\+"/g<CR><CR><C-W>hggn
nnoremap <leader>A ^f'l"zyi'/<C-R>z<CR><C-W>hggn
"nnoremap <leader>A ^f'l"zyi'q/i<C-R>z<ESC>0f"lci".\+<CR><C-W>hggn
nnoremap <leader>d ^W"zy$q/"zp:s/\v"[^"]+"/".\\+"/g<CR><CR><C-W>lggn
nnoremap <leader>D ^W"zy$/<C-R>z<CR><C-W>lggn
nnoremap <leader>c $4F.hhv^2W"zyq/"zp:s/\v"[^"]+"/".\\+"/g<CR><CR><C-W>hggn
nnoremap <leader>C $^2W"zy$<C-W>hggq/"zp<CR>
"nnoremap <leader>D ^W"zy$q/"zp0f"lci".\+<CR><C-W>lggn
" For bash file
" nnoremap <leader>v yy<C-W>jGo<CR>P0cw# 
" nnoremap <leader>g yy<C-W>lGo<CR>P0cw# 
" Convert to correct format
" :%s/\v^(Given|When|Then)\n {4}(.*)$/\1 \2/
" :%s/\v^ {4}(.*)$/    And \1/
" :%s/\v^(Given|When|Then|    And)(.*)$/    \0/
" Copy task to below (v) or right (g) window
" In case more automation is needed
" nnoremap <leader>v ^y$<C-W>jo<CR><CR><ESC>p0i@<ESC>l~els(u'<ESC>A'<ESC>hvi':s/ /_/g<CR>lyi'A)<CR>def (context):<CR><ESC>kP
nnoremap <leader>v $?\v<(given\|when\|then)><CR>"zye``^wy$<C-W>jo<ESC>i<CR><CR>@<ESC>"zpb~ea(u'')<ESC>Tupodef step_impl(context):<CR># Test model<CR>Test UI<CR>raise NotImplementedError(u'STEP:  ')<ESC>F "zPlp
nnoremap <leader>g $?\v<(given\|when\|then)><CR>"zye``^wy$<C-W>lo<ESC>i<CR><CR>@<ESC>"zpb~ea(u'')<ESC>Tupodef step_impl(context):<CR># Test model<CR>Test UI<CR>raise NotImplementedError(u'STEP:  ')<ESC>F "zPlp

au BufNewFile,BufRead *.feature
    \ set foldlevel=1 |
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix |
