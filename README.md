The story
=========

Ho Chi Minh City, Vietnam, some day in 1987. The distance from bus stop to my
secondary school was about half a mile. I was walking at noon and enjoying the
shading of tall big trees along the way. It was hot but the air was fresh.

At a bus station in Folsom, USA, October 2013. After a working day in office, I
was waiting for a bus to go back to hotel. The autum air was quite hot and I
felt it in every breath. Then I saw a message at bus stop's panel.

!["Thanks for taking the bus today. It's a huge help.  Our job has gotten so difficult lately - it's like trying to avoid the wind.  We can't tell you what a breath of fresh air it is that you're riding the bus.  Every time you do, it totally helps us breathe easier, which of course, helps you do the same.  From the bottom of our roots, thank you.  The Trees"][message_from_trees]

[message_from_trees]: https://dxmt.mocdiep.com/message_from_trees_480x851.jpg

Da Nang, Vietnam, March 2020. The weather was so burning at several places.
After first few days people stayed at home due to COVID-19 pandemic in Vietnam,
there was sudden rain and it cooled the city. The rain was at strange period
that had not been seen recent years.

To me, the environment has been changed too much after 33 years.
[Al Gore, former U.S. vice president, spoke about Climate Crisis (Wikipedia)].
Extreme weather will harm all species on Earth if we do not take action now.

[Al Gore, former U.S. vice president, spoke about Climate Crisis (Wikipedia)]: https://en.wikipedia.org/wiki/Al_Gore

So join us to live happily and peacefully with our Mother Earth.

Objective
=========

Public transportation is the most effective way for daily commuting. To help
commuters understand their impact to global environment, this application keeps
track of their milage in term of planted trees. It is inspired by [Ecosia], the
search engine that plants trees. This application provides time table at each
bus stop so that commuters can maximize usage of their time at needed bus stop.

[Ecosia]: https://www.ecosia.org/

Installation
============

```bash
ENV_FOLDER=env_woodbus
python3 -m venv ${ENV_FOLDER} && source ${ENV_FOLDER}/bin/activate && pip install --upgrade pip

git clone git@gitlab.com:mocdiepvn/woodbus.git
# or git clone https://gitlab.com/mocdiepvn/woodbus.git
cd woodbus
ln -s ../${ENV_FOLDER} env
pip install -r requirements.txt

# For development
pip install -r requirements_dev.txt

# Update woodbus/settings.py with neccessary languages and countries
# To load latest data
python3 manage.py cities_light

# To save imported data
mkdir -p var/fixtures
cp /path/to/data/fixtures/* var/fixtures
dj cities_light_fixtures dump

# To use existing data
dj cities_light_fixtures load
```

Development Processes
=====================

Concurrently develop requirements and features
----------------------------------------------

* Why: save time, let developers to involve in planning phase
* Steps:
    * Branch `features/reqs` from `develop` for BA to work on feature
      files
    * Branch `features/<feature_name>` from `develop` for Developer to work on
      steps files
    * Merge `develop` with `features/reqs` for latest requirements
    * Diff `features/<feature_name>` with `develop` then merge for updates
        * Problem when moving files to another folder --> lost text in original
        file
        * Only rename/move files after merging all branchs to `develop`, then
          all branches should start from `develop` again
    * Merge `develop` with `features/<feature_name>` for latest feature

Behavior Driven Development
---------------------------

### Business Analyst

* Edit feature files in `features/` folder
* To update a step in feature file: duplicate the line, comment one and change the other one, delete the old one when developer confirm the change is implemented on step files.

### Developer

* Edit step files in features/steps folder then edit source codes, templates
* Keep the steps those are not being work in current sprint out of step files
* With vim
    * Put into ~/.vimrc: `nnoremap <F4> :so .vimrc<cr>`
    * Put `@wip` to Scenario that is being worked, optionally related ones
    * Press `F4` to run test for Scenarios with `@wip`
    * Press `<leader>F4` to run all tests
    * Arrange feature file as left (or above) window, select step that does not
        exist in step file, arrange step file as right (or below) window,
        select end line of a step, then press `<leader>g` (or `<leader>v)` to
        insert the selected step from feature file to step file.

```bash
./djbh.sh
```

* Behave test output from `./djbh.sh` above or `<leader>F4` in vim
```
0 features passed, 1 failed, 0 skipped
12 scenarios passed, 6 failed, 0 skipped
73 steps passed, 0 failed, 7 skipped, 11 undefined
Took 0m0.597s
```

* Output explaination
```
0 features passed, 1 failed, 0 skipped
                   ^---------^--------------- kanban
12 scenarios passed, 6 failed, 0 skipped
                     ^---------^------------- kanban
73 steps passed, 1 failed, 7 skipped, 10 undefined
                 ^         ^----------^------ kanban
                 +--------------------------- wip
Took 0m0.597s
```

* Make sure 100% coverage so that all lines of code can be executed properly
    when needed.

```bash
DJANGO_SETTINGS_MODULE=woodbus.dev_settings coverage run; coverage html
browse htmlcov/index.html
```

Rearrange code without impacting works of Developers and BAs
------------------------------------------------------------

* Why: rename/move files may cause losing code when merging feature branches to
    `develop`
* Steps:
    * mv001: Merge `develop` with all feature branches
    * mv002: Perform rename/move files as needed
    * mv003: Start all branches from the `develop`

References
==========

* [Getting a Django Application to 100% Coverage]
* [robmoggach/django-token-auth]
* [anselmlingnau/django-language-flags] and [ktalik/django-country-and-language-flags] --> [Country Flag Sprites]
* [How to Write Terms and Conditions]
* [Open source template: Terms Of Use]

[Getting a Django Application to 100% Coverage]: https://adamj.eu/tech/2019/04/30/getting-a-django-application-to-100-percent-coverage/
[robmoggach/django-token-auth]: https://github.com/robmoggach/django-token-auth
[anselmlingnau/django-language-flags]: https://github.com/anselmlingnau/django-language-flags
[ktalik/django-country-and-language-flags]: https://github.com/ktalik/django-country-and-language-flags/network
[Country Flag Sprites]: https://flag-sprites.com/
[How to Write Terms and Conditions]: https://www.wikihow.com/Write-Terms-and-Conditions
[Open source template: Terms Of Use]: http://opensource-template.wikidot.com/legal:terms-of-use
[KosmosJournal: Thich Nhat Hanh's Code of Global Ethics]: https://www.kosmosjournal.org/kj_article/thich-nhat-hanhs-code-of-global-ethics/
[PlumVillage: The Five Mindfulness Trainings]: https://plumvillage.org/mindfulness-practice/the-5-mindfulness-trainings/
[PlumVillage: Use of ‘cookies’]: https://plumvillage.org/privacy-policy/#cookies
