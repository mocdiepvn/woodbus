# features/count_trees.feature
# features/steps/hash_email_auth.py

Feature:
    As a privacy caring person I want to keep my email address secret so that
    no one can get it from database.

    # (1) Account: no / yes
    # (2) Logged in from device A: no / yes=(4).no
    # (3) Logged in from device B: no / yes
    # (4) (2) Expired: no / yes

    Scenario Outline: creating user with details
        When create user with username "Moc Diep", email "<email>", password "<password>"
        Then "<num_user>" user is created with username "Moc Diep" "<email_state>" email "<passwd_state>" password

    Examples: credentials
    | email                 | password      | num_user  | email_state   | hashed_state  | passwd_state  |
    | mocdiep@mocdiep.com   | None          | one       | without       | hashed        | without       |
    | mocdiep@mocdiep.com   | password      | one       | with          | hashed        | with          |
    | None                  | password      | no        | N/A           | N/A           | N/A           |
    | None                  | None          | no        | N/A           | N/A           | N/A           |

    Scenario: not logged in user opens home page
        Given user has no account with "user@company.com" on system
        And user with "user@company.com" has not logged in to system from device "A"
        When user opens home page from device "A"
        Then the page at device "A" has intercooler link to get authentication

    Scenario: intercooler at 1st device without logged in user gets authentication status
        Given user has no account with "user@company.com" on system
        And user with "user@company.com" has not logged in to system from device "A"
        When intercooler at device "A" open link to get authentication
        Then the page at device "A" has login form

    Scenario Outline: user provides incorrect email in login form
        Given user with "user@company.com" has not logged in to system from device "A"
        When intercooler at device "A" open link to get authentication
        And user provides "<incorrect_email>" email in login form at device "A"
        Then response at device "A" has incorrect email message

    Examples: emails
            | incorrect_email           |
            | incorrect                 |
            | incorrect@domain          |
            #| not@exist.domain.123456   |
            #| not_exist@mocdiep.com     |

    Scenario: user provides correct email in login form
        # (1).no, (2).no, (3).same with 2
        Given user has no account with "user@company.com" on system
        And user with "user@company.com" has not logged in to system from device "A"
        When intercooler at device "A" open link to get authentication
        And user provides "user@company.com" email in login form at device "A"
        Then response at device "A" has email sent message
        And user received email with one-time link for device "A"

    Scenario: user requests twice but should only receive one token
        Given user has no account with "user@company.com" on system
        And user with "user@company.com" has not logged in to system from device "A"
        When intercooler at device "A" open link to get authentication
        And user provides "user@company.com" email in login form at device "A"
        And intercooler at device "B" open link to get authentication
        And user provides "user@company.com" email in login form at device "B"
        Then response at device "A" has email sent message
        And user with "user@company.com" has "1" login token
        And user received email with one-time link for device "A"

    Scenario: user has expired token requests login token again
        Given user has no account with "user@company.com" on system
        And user with "user@company.com" has not logged in to system from device "A"
        And user with "user@company.com" has expired login token
        When intercooler at device "A" open link to get authentication
        And user provides "user@company.com" email in login form at device "A"
        Then user with "user@company.com" has "2" login token
        And user received email with one-time link for device "A"

    Scenario: user clicks one-time link in email at device A
        # -> (1).yes, (2).yes, (3).no
        Given user has no account with "user@company.com" on system
        And user with "user@company.com" has not logged in to system from device "A"
        And user with "user@company.com" has not logged in to system from device "B"
        When intercooler at device "A" open link to get authentication
        And user provides "user@company.com" email in login form at device "A"
        And user clicks one-time link in login email on device "A"
        Then there is active account with hashed "user@company.com" on system
        And user is logged in device "A" with account for email "user@company.com"
        And response in device "A" has redirect link
        And user is not logged in device "B" with account for email "user@company.com"

    Scenario: intercooler at 2nd device without logged in user gets authentication status
        # (1).yes, (2).yes, (3).no
        Given user has account with hashed "user@company.com" on system
        And user with "user@company.com" has logged in to system from device "A"
        And user with "user@company.com" has not logged in to system from device "B"
        When intercooler at device "B" open link to get authentication
        Then the page at device "B" has login form

    Scenario: user clicks one-time link in email at device B
        # -> (1).yes, (2).yes, (3).yes
        Given user has account with hashed "user@company.com" on system
        And user with "user@company.com" has logged in to system from device "A"
        And user with "user@company.com" has not logged in to system from device "B"
        When intercooler at device "B" open link to get authentication
        And user provides "user@company.com" email in login form at device "B"
        And user clicks one-time link in login email on device "B"
        Then there is no new account created
        And user is logged in device "A" with account for email "user@company.com"
        And user is logged in device "B" with account for email "user@company.com"

    Scenario Outline: intercooler at both device with logged in user gets authentication status
        # (1).yes, (2).yes, (3).yes
        Given user has account with hashed "user@company.com" on system
        And user with "user@company.com" has logged in to system from device "A"
        And user with "user@company.com" has logged in to system from device "B"
        When intercooler at device "<device>" open link to get authentication
        Then the page at device "<device>" has account info of "user@company.com"

    Examples: devices
            | device    |
            | A         |
            | B         |

    Scenario: user clicks logout link at one device
        # (1).yes, (2).yes, (3).yes
        Given user has account with hashed "user@company.com" on system
        And user with "user@company.com" has logged in to system from device "A"
        When user at device "A" click logout link
        Then the page at device "A" has redirect instruction for ic

    Scenario: user clicks logout link at one device and still be logged in at other device
        # (1).yes, (2).yes, (3).yes
        Given user has account with hashed "user@company.com" on system
        And user with "user@company.com" has logged in to system from device "A"
        And user with "user@company.com" has logged in to system from device "B"
        When user at device "A" click logout link
        And intercooler at device "A" open link to get authentication
        And intercooler at device "B" open link to get authentication
        Then the page at device "A" has login form
        And the page at device "B" has account info of "user@company.com"

    # Scenario: user cannot search for account without published email
    #     # (1).yes, (2).yes, (3).yes
    #     Given user has account with hashed "user@company.com" on system
    #     When user searches for email "user@company.com"
    #     Then user gets no account

    # Scenario: user can search for account with published email
    #     # (1).yes, (2).yes, (3).yes
    #     Given user has account with hashed "user@company.com" on system
    #     And user has preference with email "user@company.com"
    #     When user searches for email "user@company.com"
    #     Then user gets the account with hashed "user@company.com" on system
