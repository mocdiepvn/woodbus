# features/steps/bus_time.py

Feature:
    As a Green Bus Rider I want to know bus arrival time at any bus stop
    so that I can better plan for my time at the stops.

    Background: system has bus structure and schedules
        Given system has bus structure
        And system has bus schedules

    Scenario: user opens home page then have search feature
        When user opens home page without query value
        Then result has search input url for ic with no query value

# No query value:
# +-----------------------------------------+
# | Home page                               |
# |    <ic search input url>                |
# |                                         |
# |       <ic search url>                   |
# |                                         |
# +-----------------------------------------+

    Scenario: user opens url search input
        When user opens url search input
        Then result has ic url for search input

    Scenario: intercooler gets search input without query value
        When intercooler uses url to get search input without query value
        Then result has search form without default value

# with query value
# /?q=...
# +-----------------------------------------+
# | Home page                               |
# |    <ic search input url/?q=...>         |
# |                                         |
# |       <ic search url/?q=...>            |
# |                                         |
# +-----------------------------------------+

    Scenario: user opens home page with query value
        When user opens home page with query value of "khiêm"
        Then result has search input url for ic with query value of "khiêm"
        # ic url: ic-post-to="<url>"; ic-get-from="<url>"
        And result has search result url for ic with query value of "khiêm"

    Scenario: intercooler gets search input with query value
        When intercooler uses url to get search input with query value of "vương"
        Then result has search input with "vương"

    Scenario: user opens detail url for a street
        When user opens detail url for a street "da-nang-hung-vuong"
        Then result has ic detail url for street "da-nang-hung-vuong"

    Scenario: intercooler opens detail url for a street
        When intercooler uses url to get detail for a street "da-nang-ong-ich-khiem"
        # https://behave.readthedocs.io/en/latest/api.html#step-macro-calling-steps-from-other-steps
        #Then result has details for street "da-nang-ong-ich-khiem"

    Scenario: user searches with empty string
        When user searches for empty string
        Then result has div "message" "Cần nhập tiêu chí tìm kiếm khác rỗng"

    Scenario: user searches with empty string
        When user searches for "..."
        Then result has div "message" "Không tìm thấy kết quả"
    Scenario: user searches for streets
        When user searches for "vương"
        Then result has "Street" "Đà Nẵng - Hùng Vương"
        And result has "Street" "Đà Nẵng - Trưng Nữ Vương"
        And result has "Street" "Tp. Hồ Chí Minh - Hùng Vương"
        And result has "Street" "Tp. Hồ Chí Minh - An Dương Vương"
        And result has "BusRoute" "Xuân Diệu - Siêu thị Lotte - CĐ Việt Hàn"
        And result has "BusRoute" "Bến xe trung tâm Đà Nẵng - Bến xe Ái Nghĩa"

    Scenario: user searches for routes
        When user searches for "Nẵng - Bến"
        Then result has no "Street"
        And result has "BusRoute" is "Bến xe trung tâm Đà Nẵng - Bến xe Ái Nghĩa"

    Scenario: user selects street to view bus stops
        When user searches for "khiêm"
        And user selects "Street" "Đà Nẵng - Ông Ích Khiêm"
        Then result has main street "da-nang-ong-ich-khiem"
        And result has "even" side bus stop "Trước 8" with routes "DNG_03"
        And result has "even" side bus stop "Trước 36" with routes "DNG_03, DNG_11"
        And result has "even" side bus stop "Trước 144" with routes "DNG_03"
        And result has "even" side bus stop "Trước 162" with routes "DNG_03"
        And result has "even" side bus stop "Đối diện CĐ Công nghệ" with routes "DNG_11"
        And result has "even" side bus stop "Công viên" with routes "DNG_11"
        And result has "odd" side bus stop "Đối diện 36" with routes "DNG_03"
        And result has "odd" side bus stop "Trước 79" with routes "DNG_03, DNG_11"
        And result has "odd" side bus stop "Trước 417-419" with routes "DNG_03"
        And result has "odd" side bus stop "Trường CĐ Công nghệ" with routes "DNG_11"
        And result has "3" "previous" stops on route
        And result has end div logic in stops on route
        And result has route "DNG_11" with "previous" street "1" is "Nguyễn Tất Thành"
        And result has route "DNG_03" with "previous" street "2" is "Nguyễn Tất Thành"
        And result has route "DNG_03" with "previous" street "3" is "Hùng Vương"
        And result has route "DNG_11" with "next" street "1" is "Hải Phòng"
        And result has route "DNG_03" with "next" street "2" is "Lê Duẫn"
        And result has route "DNG_03" with "next" street "3" is "Hoàng Diệu"
        # TODO: need to test for first and last streets
        # And result has route "DNG_03" with "first" street "1" is "Tôn Đức Thắng"
        # And result has route "DNG_11" with "first" street "3" is "Xuân Diệu"
        # And result has route "DNG_03" with "last" street "1" is "Trưng Nữ Vương"
        # And result has route "DNG_11" with "last" street "3" is "Phạm Văn Nghị"

    Scenario: user selects street to view bus stops
        When user searches for "thành"
        And user selects "Street" "Đà Nẵng - Nguyễn Tất Thành"
        Then result has main street "da-nang-nguyen-tat-thanh"
        And result has "even" side bus stop "Đối diện 39" with routes "DNG_11"
        And result has "even" side bus stop "Trước 714" with routes "DNG_03"
        And result has "odd" side bus stop "Trước 617" with routes "DNG_03"
        And result has "odd" side bus stop "Trước 61" with routes "DNG_11"

    Scenario: user selects last street to view bus stops
        When user selects "Street" "Đà Nẵng - Trưng Nữ Vương"
        Then result has "even" side bus stop "Trước 308" with routes "DNG_03"
        And result has "odd" side bus stop "Trước 233" with routes "DNG_03"
        And result has route "DNG_03" with "next" street "1" is "Hoàng Diệu"

    Scenario: user selects last street to view bus stops
        When user selects "Street" "Đà Nẵng - Trần Cao Vân"
        Then result has "return" side bus stop "Trước 716" with routes "DNG_03"

    Scenario: user selects a bus route
        When user selects a bus route "DNG_11"
        Then result has "14" bus stops of "forward" street
        And result has "13" bus stops of "return" street
        # 1 step for 3 sor + 3 arrived time of forward
        # Then result has "XE_13, XE_15, XE_17" with "09:15, 09:16, 09:19" of "forward"
        # 1 step for 3 sor + 3 arrived time of return
        # And result has "XE_14, XE_16, XE_18" with "09:15, 09:20, 09:19" of "return"
        # 1 step for 3 sor + 3 eta of forward
        # 1 step for 3 sor + 3 eta of return
        And result has rowspan counts for bus stops of streets in bus route

    Scenario: user selects a bus route
        When current time is "09:21"
        And user selects a bus route "DNG_11"
        Then result has "9" buses not reached bus stop with direction "forward"
        And result has "9" buses not reached bus stop with direction "return"

    Scenario: user selects at time at home page
        When user selects at time "10:00" at home page
        Then result has at time "10:00" at home page

    Scenario: user selects at time with a bus route
        When user selects at time "9:21" with bus route "DNG_11"
        Then result has "9" buses not reached bus stop with direction "forward"
        And result has "9" buses not reached bus stop with direction "return"

    Scenario: user selects street to view bus stops
        When user searches for "khiêm"
        And user selects "BusRoute" has name "Xuân Diệu - Siêu thị Lotte - CĐ Việt Hàn" has id "DNG_11"
        Then result has main bus route "DNG_11"
        And result has "forward" street and bus stop of street
        And result has "return" street and bus stop of street

    Scenario: user selects first street
        When user selects "Street" "Đà Nẵng - Tôn Đức Thắng"
        Then result has route "DNG_03" with "previous" street "1" is "Điện Biên Phủ"
        And result has "even" side bus stop "Bến xe Đà Nẵng" with routes "DNG_03"

    Scenario: user selects last street
        When user selects "Street" "Đà Nẵng - Xuân Diệu"
        Then result has route "DNG_11" with "next" street "1" is "3 tháng 2"
        And result has "even" side bus stop "Trạm xe buýt Xuân Diệu" with routes "DNG_11"

    Scenario: user opens detail url for a bus stop
        When user opens detail url for a bus stop "TRAM_36"
        Then result has ic detail url for bus stop "TRAM_36"

    Scenario: user selects bus stop to view detail
        When current time is "08:01"
        And user selects bus stop "Trước 162"
        Then result has bus stop and street "Ông Ích Khiêm"
        And result has route "1" "DNG_03" with direction "Di"
        And result has bus "XE_7" with estimated time "08:02"
        And result has bus "XE_9" with estimated time "08:32"
        And result has bus "XE_11" with estimated time "09:02"
        And result has bus "XE_13" with estimated time "09:32"
        And result has bus "XE_15" with estimated time "10:02"

    Scenario: user selects bus stop to view detail
        When current time is "08:03"
        And user selects bus stop "Trước 162"
        Then result has bus stop and street "Ông Ích Khiêm"
        And result has route "1" "DNG_03" with direction "Di"
        And result has bus "XE_9" with estimated time "08:32"
        And result has bus "XE_11" with estimated time "09:02"
        And result has bus "XE_13" with estimated time "09:32"
        And result has bus "XE_15" with estimated time "10:02"
        And result has bus "XE_17" with estimated time "10:32"

    Scenario: user selects bus stop to view detail
        When current time is "09:21"
        And user selects bus stop "Trước 79"
        Then result has bus stop and street "Ông Ích Khiêm"
        And result has route "1" "DNG_03" with direction "Ve"
        And result has bus "XE_16" with estimated time "09:36"
        And result has bus "XE_18" with estimated time "10:06"
        And result has bus "XE_20" with estimated time "10:36"
        And result has bus "XE_22" with estimated time "11:06"
        And result has bus "XE_24" with estimated time "11:36"
        And result has route "2" "DNG_11" with direction "Ve"
        And result has bus "XE_16" with estimated time "09:26"
        And result has bus "XE_18" with estimated time "09:46"
        And result has bus "XE_20" with estimated time "10:06"
        And result has bus "XE_22" with estimated time "10:26"
        And result has bus "XE_24" with estimated time "10:46"

    Scenario: user selects bus stop to view detail
        When current time is "09:37"
        And user selects bus stop "Trước 79"
        Then result has bus stop and street "Ông Ích Khiêm"
        And result has route "1" "DNG_03" with direction "Ve"
        And result has bus "XE_18" with estimated time "10:06"
        And result has bus "XE_20" with estimated time "10:36"
        And result has bus "XE_22" with estimated time "11:06"
        And result has bus "XE_24" with estimated time "11:36"
        And result has bus "XE_26" with estimated time "12:06"
        And result has route "2" "DNG_11" with direction "Ve"
        And result has bus "XE_18" with estimated time "09:46"
        And result has bus "XE_20" with estimated time "10:06"
        And result has bus "XE_22" with estimated time "10:26"
        And result has bus "XE_24" with estimated time "10:46"
        And result has bus "XE_26" with estimated time "11:06"

    Scenario: user selects bus stop to view detail
        When current time is "08:08"
        And user selects bus stop "Trước 36"
        Then result has bus stop and street "Ông Ích Khiêm"
        And result has route "1" "DNG_03" with direction "Di"
        And result has bus "XE_9" with estimated time "08:22"
        And result has bus "XE_11" with estimated time "08:52"
        And result has bus "XE_13" with estimated time "09:22"
        And result has bus "XE_15" with estimated time "09:52"
        And result has bus "XE_17" with estimated time "10:22"
        And result has route "2" "DNG_11" with direction "Di"
        And result has bus "XE_9" with estimated time "08:12"
        And result has bus "XE_11" with estimated time "08:32"
        And result has bus "XE_13" with estimated time "08:52"
        And result has bus "XE_15" with estimated time "09:12"
        And result has bus "XE_17" with estimated time "09:32"

    Scenario: user selects bus stop to view detail
        When current time is "09:21"
        And cache stat is all zeroes
        And user selects bus stop "Trước 36"
        And user selects bus stop "Trước 36"
        And user selects bus stop "Trước 36"
        Then cache missed is "2" and cache hit is "4"

    Scenario: user selects bus stop to view detail
        When current time is "09:21"
        And user selects bus stop "Trước 36"
        Then result has bus stop and street "Ông Ích Khiêm"
        And result has route "1" "DNG_03" with direction "Di"
        And result has bus "XE_13" with estimated time "09:22"
        And result has bus "XE_15" with estimated time "09:52"
        And result has bus "XE_17" with estimated time "10:22"
        And result has bus "XE_19" with estimated time "10:52"
        And result has bus "XE_21" with estimated time "11:22"
        And result has route "2" "DNG_11" with direction "Di"
        And result has bus "XE_17" with estimated time "09:32"
        And result has bus "XE_19" with estimated time "09:52"
        And result has bus "XE_21" with estimated time "10:12"
        And result has bus "XE_23" with estimated time "10:32"
        And result has bus "XE_25" with estimated time "10:52"

    Scenario: user selects bus stop to view detail
        When current time is "13:21"
        And user selects bus stop "Trước 36"
        Then result has bus stop and street "Ông Ích Khiêm"
        And result has route "1" "DNG_03" with direction "Di"
        And result has bus "XE_29" with estimated time "13:22"
        And result has bus "XE_31" with estimated time "13:52"
        And result has bus "XE_33" with estimated time "14:22"
        And result has bus "XE_35" with estimated time "14:52"
        And result has bus "XE_37" with estimated time "15:22"
        And result has route "2" "DNG_11" with direction "Di"

    Scenario: user selects correct timezone
        When user selects time zone "Asia/Ho_Chi_Minh" with street slug "da-nang-ong-ich-khiem"
        Then result has time zone "Asia/Ho_Chi_Minh"

    Scenario: user selects incorrect timezone
        When user selects time zone "Asia/Time_zone" with street slug "da-nang-ong-ich-khiem"
        Then result has time zone "None"

    Scenario: user selects incorrect timezone
        When user selects time zone "Asia/Ho_Chi_Minh" with street slug "da-nang-ong-ich-khiem"
        And user selects time zone "Timezone" with street slug "da-nang-ong-ich-khiem"
        Then result has time zone "Asia/Ho_Chi_Minh"

    Scenario: user selects correct timezone with at time
        When user selects at time "10:00" at home page
        And user selects time zone "Asia/Ho_Chi_Minh" with street slug "da-nang-ong-ich-khiem"
        Then result has at time "10:00"

    Scenario: user selects correct timezone twice with at time
        When user selects at time "10:00" at home page
        And user selects time zone "Asia/Ho_Chi_Minh" with street slug "da-nang-ong-ich-khiem"
        And user selects time zone "UTC" with street slug "da-nang-ong-ich-khiem"
        Then result has at time "10:00"

    Scenario: user selects incorrect timezone
        When user selects time zone "Asia/Ho_Chi_Minh" with street slug "da-nang-ong-ich-khiem"
        And user selects time zone "''" with street slug "da-nang-ong-ich-khiem"
        Then result has time zone "Asia/Ho_Chi_Minh"
