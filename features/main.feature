# features/steps/main.py

Feature:
    User opens home page and have neccessary controls to interact with
    the site. Developer uses common functions.

    Scenario Outline: user can change language
        When user opens home page in "<language>" language
        Then the page has languages as in settings with "<language>" language as active

    Examples:
        | language  |
        | en        |
        | vi        |

    Scenario Outline: user can view general pages
        When intercooler uses url to get "<page>" page
        Then result has "<page>" content

    Examples:
        | page                  |
        | Contributors          |
        | Translations          |
        | WIP                   |
        | Terms And Conditions  |

    Scenario: annonymous user view terms and conditions first time
        When intercooler uses url to get "first Terms And Conditions" page
        Then result "has" refresh instruction

    Scenario: logged in user view terms and conditions
        Given user has account with hashed "user@company.com" on system
        And user with "user@company.com" has logged in to system from device "A"
        When user opens home page without query value
        And cookie has csrftoken
        And intercooler uses url to get "Terms And Conditions" page
        Then result has "Terms And Conditions" content
