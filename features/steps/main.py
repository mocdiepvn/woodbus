from behave import given, when, then
from django.urls import reverse
from django.utils.translation import activate

from main.views import get_home_url, settings, get_available_languages


def repeat(count, context, action, *args, **kwargs):
    for i in range(count):
        if i == 1:
            context.prev_ui_results = []
        else: pass
        if i > 0 and count > 1:
            context.prev_ui_results.append(context.ui_result)
        else: pass
        context.ui_result = action(*args, **kwargs)


@when(u'user opens home page in "{language}" language')
def step_impl(context, language):
    # Not Test for model
    # Test UI
    activate(language)
    url = reverse(get_home_url())
    #print('url:', url)
    context.ui_result = context.test.client.get(url)


@then(u'the page has languages as in settings with "{language}" language as active')
def step_impl(context, language):
    # Not Test model
    # Test UI
    LANGUAGES = get_available_languages(language)
    #print('step_impl', LANGUAGES)
    message = 'Active language must be the first in list'
    context.test.assertEqual(language, LANGUAGES[0][0], message)

    class_tmpl = 'class="flag flag-{{ flag }}"'
    active_lang_id = ' id="img_active_language"'
    result = []
    for lang_code, flag in LANGUAGES:
        expecting = class_tmpl.replace('{{ flag }}', flag)
        if lang_code == language:
            expecting += active_lang_id
        else: pass
        #print(class_tmpl, expecting)
        try:
            context.test.assertContains(context.ui_result, expecting, 1)
        # Only run this when test failed so no need coverage checking
        except Exception: continue  # pragma: no cover
        result.append((lang_code, flag))
    context.test.assertEqual(LANGUAGES, result)


def check_cookie_sessions(context):
    response = context.ui_result
    client = context.test.client
    session = client.session.__dict__
    #print('check_cookie_sessions', client.cookies, client.session.__dict__)
    #print(response.request)
    #print('sessionid', client.cookies['sessionid'].value)
    context.test.assertContains(response, client.cookies['sessionid'].value)
    context.test.assertNotContains(response, 'test_csrftoken')
    #context.test.assertEqual(response.context['session'], {
    #        '_SessionBase__session_key': session['_SessionBase__session_key'],
    #        'accessed': session['accessed'],
    #        'modified': session['modified'],
    #        })
    assert len(context.prev_ui_results) == 1
    check_refresh_instruction(context, 'has no')


@then(u'result "{has_refresh}" refresh instruction')
def check_refresh_instruction(context, has_refresh):
    # Test model
    # Test UI
    response = context.ui_result
    if has_refresh == 'has':
        assert_has = context.test.assertContains
    else:
        assert_has = context.test.assertNotContains
    assert_has(response, "Please refresh the page to see cookie."
            + " If still not see, please allow it using browser's settings.")


url_names = {
        'Terms And Conditions': ('terms_and_conds', 2, check_cookie_sessions),
        'first Terms And Conditions': ('terms_and_conds', 1, None),
        }


@when(u'intercooler uses url to get "{page_name}" page')
def step_impl(context, page_name):
    # Not Test model
    # Test UI
    #print(dict(context.test.client.session), context.test.client.cookies)
    activate(settings.LANGUAGE_CODE)
    #client = context.test.client
    #session = client.session.__dict__
    #print('step_impl', client.cookies, client.session.__dict__)
    try:
        url_name, count, context.check_more = url_names[page_name]
    except KeyError:
        url_name, count, context.check_more = page_name.lower(), 1, None
    repeat(count, context, context.test.client.get, reverse(url_name))
    #print('after', dict(context.test.client.session), context.test.client.cookies)


@when(u'cookie has csrftoken')
def step_impl(context):
    # Test model
    # Test UI
    client = context.test.client
    client.cookies['csrftoken'] = 'test_csrftoken'


@then(u'result has "{page_name}" content')
def step_impl(context, page_name):
    # Not Test model
    # Test UI
    #print(context.ui_result.content.decode('utf-8'))
    headers = {
            'Terms And Conditions': 'h2',
            }
    try:
        header = headers[page_name]
    except KeyError:
        header = 'h1'
    if page_name == 'WIP':
        context.test.assertContains(context.ui_result, 'Work In Progress')
    else:
        context.test.assertContains(context.ui_result,
                '<{1}>{0}'.format(page_name, header), 1)
    check_more = context.check_more
    if check_more:
        check_more(context)
    else: pass
