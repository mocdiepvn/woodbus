from behave import given, when, then
from behave_django.decorators import fixtures
# https://docs.djangoproject.com/en/2.2/topics/auth/customizing/#referencing-the-user-model
from django.contrib.auth import get_user_model
from django.forms.models import model_to_dict
from django.urls import reverse
from django.utils import timezone
from django.utils.text import slugify
from unidecode import unidecode

#from hash_email_auth import models as authmdl
from bus_time import models as busmdl

User = get_user_model()


# Software development Stages:
# Developing: local pc <-- bus_structure.json
# Staging: test server on internet <-- data from hkxb
# Production: working server on internet
# Option 1: Self defined
# Option 2: Simple, being maintained
# [yourlabs/django-cities-light]: https://github.com/yourlabs/django-cities-light/issues
# Option 3: Too old
# [django-cities-tiny]: https://pypi.org/project/django-cities-tiny/#history
# Option 4: Heavy, Not maintained
# [coderholic/django-cities]: https://github.com/coderholic/django-cities/issues

# [pypi.org> search> django locations]: https://pypi.org/search/?q=django+locations&o=
# To select location
# [caioariede/django-location-field]: https://github.com/caioariede/django-location-field
# [Simon-the-Shark/django-mapbox-location-field]: https://github.com/Simon-the-Shark/django-mapbox-location-field
# To locate stores near a location
# [MegaMark16/django-cms-storelocator]: https://github.com/MegaMark16/django-cms-storelocator
# [djangocms-store-locator 0.1.3]: https://pypi.org/project/djangocms-store-locator/


# https://behave-django.readthedocs.io/en/latest/usage.html#fixture-loading
# Modify script for generating xuat_json to include some cities and have
# bus_time_street.city_id point to a city
# Save file xuat_json as bus_structure.json
@fixtures('features/bus_structure.json')
#@fixtures('features/bus_structure_copy.json')
@given(u'system has bus structure')
def has_bus_structure(context):
    # system has list of cities
    # each city has list of streets
    #print(busmdl.Street.objects.all())
    #context.test.assertEqual(2, authmdl.User.objects.count())
    context.test.assertEqual(2, User.objects.count())
    bus_route = busmdl.BusRoute.objects.all()
    #print('bus stop', bus_route)
    st = busmdl.Street.objects.all()
    #print('street', st)
    bus_company = busmdl.BusCompany.objects.all()
    context.test.assertEqual(61, busmdl.BusStop.objects.count())
    bus_stop = busmdl.BusStop.objects.all()
    #print('tt search', busmdl.search('vương'))
    #list_bus_top = [line for line in bus_stop]
    #print('list bus top: ', list_bus_top)
    # each street has list of bus stops
    # each city has list of operating bus companies
    # each bus company has list of bus routes
    # each bus route has two bus directions
    # each bus direction has list of bus stops
    self = context.test
    #print(*dir(self), sep='\t')
    #self.assertEqual('a', 'b')
    name_street = bus_stop[0].street
    #assert 3 == st.count()
    assert 'Đà Nẵng - Xuân Diệu' == str(name_street)

# Not contain anything from bus_structure.json
@fixtures('features/bus_schedule.json')
@given(u'system has bus schedules')
def has_bus_schedules(context):
    # each bus direction has list of start times with intervals
    # each bus direction has list of times between stops for each start time
    assert True


# What is the purpose of function? Reuse
# How to reuse? call the function with parameter.
# First run:
# expecting = ['a', 'b', 'c]
# result = assert_expecting(expecting)
# Second run:
# expecting = ['c', 'd', 'e']
# result = assert_expecting(expecting)
# Variable: Generalize/Summary/Categorize
# Wave, Grande --> Bike
# Paremeter --> Variable
def assert_contain(context, expecting, response):
    '''
    Decode content of client, then check if decoded content has strings in
    expecting list.
    '''
    # First run, expecting = ['a', 'b', 'c']
    # Second run, expecting = ['c', 'd', 'e']
    ui_result = response.content.decode('utf-8')
    #print('ui_result', ui_result))
    result = [
                need_test for need_test in expecting
                if need_test in ui_result
            ]
    context.test.assertEqual(expecting, result)


@when(u'user opens home page without query value')
def step_impl(context):
    # No Test for model
    # Test for UI
    url = reverse('bus_time:index')
    #print('url:', url)
    # print('context', context)
    context.ui_result = context.test.client.get(url)


@then(u'result has search input url for ic with no query value')
def step_impl(context):
    # No Test model
    # Test UI
    # then ... url to get search input ...
    # ... with no query value
    url_need = reverse('bus_time:search_input')
    expecting = [
            # then ... ic ...
            'ic-get-from="{0}"'.format(url_need),
            'ic-trigger-on="load"',
            ]
    # print('ui_result', context.ui_result)
    assert_contain(context, expecting, context.ui_result)


@when(u'user opens url search input')
def step_impl(context):
    url = reverse('bus_time:search_input')
    # No Test model
    # Test UI
    context.ui_result = context.test.client.get(url)
    #ui_result = context.ui_result.content.decode('utf-8')
    #print('context', ui_result)


@then(u'result has ic url for search input')
def step_impl(context):
    # No Test model
    url = reverse('bus_time:search_input')
    # Test UI
    expecting = [
             'ic-get-from="{0}"'.format(url),
             'ic-trigger-on="load"',
            ]
    assert_contain(context, expecting, context.ui_result)


@when(u'intercooler uses url to get search input without query value')
def step_impl(context):
    # No Test model
    # Test UI
    url = reverse('bus_time:search_input')
    context.ui_result = context.test.client.get(url,
            data={'ic-request': 'true'})


@then(u'result has search form without default value')
def step_impl(context):
    # No Test for model
    # Test for UI
    url = reverse('bus_time:search')
    expecting = [
            'name="q"',
            'ic-get-from="{0}"'.format(url),
            'ic-trigger-on="keyup changed" ic-trigger-delay="500ms"',
            'ic-target="#page_content">',
            ]
    assert_contain(context, expecting, context.ui_result)


@when(u'user opens home page with query value of "{search_text}"')
def step_impl(context, search_text):
    #print('search_text', search_text)
    # ... with query value of ...
    url = reverse('bus_time:index') + '?q=' + search_text
    #context.ui_result_input = context.test.client.get(url_need)
    context.ui_result = context.test.client.get(url)
    #ui_result = context.ui_result.content.decode('utf-8')


@then(u'result has search input url for ic with query value of "{search_text}"')
def step_impl(context, search_text):
    # then ... url to get search input ...
    # ... with query value of ...
    url_need = reverse('bus_time:search_input') + '?q=' + search_text
    # No Test model
    # Test UI
    expecting = [
            'ic-get-from="{0}"'.format(url_need),
            'ic-trigger-on="load"',
            ]
    #ui_result = context.ui_result.content.decode('utf-8')
    #print('context', ui_result)
    assert_contain(context, expecting, context.ui_result)


@then(u'result has search result url for ic with query value of "{search_text}"')
def step_impl(context, search_text):
    url = reverse('bus_time:search') + '?q=' + search_text
    #url = reverse('bus_time:search')
    expecting = [
            'ic-get-from="{0}"'.format(url),
            'ic-trigger-on="load"',
            ]
    # No Test model
    # Test UI
    ui_result = context.ui_result.content.decode('utf-8')
    #print('context', ui_result)
    assert_contain(context, expecting, context.ui_result)


@when(u'intercooler uses url to get search input with query value of "{search_text}"')
def step_impl(context, search_text):
    # No Test model
    # Test UI
    url = reverse('bus_time:search_input')
    context.ui_result = context.test.client.get(url,
            data={'ic-request': 'true', 'q': search_text})
    #ui_result = context.ui_result.content.decode('utf-8')
    #print('context', ui_result)


@then(u'result has search input with "{search_text}"')
def step_impl(context, search_text):
    # No Test model
    # Test UI
    url = reverse('bus_time:search')
    expecting = [
             'name="q" value="{0}"'.format(search_text),
             'ic-get-from="{0}"'.format(url),
             'ic-trigger-on="keyup changed" ic-trigger-delay="500ms"',
             'ic-target="#page_content">',
             ]
    assert_contain(context, expecting, context.ui_result)


@when(u'user opens detail url for a street "{street_slug}"')
def step_impl(context, street_slug):
    url = reverse('bus_time:street_detail', args=[street_slug])
    url = reverse('bus_time:street_detail', kwargs={'street_slug': street_slug})
    #print('url_detail', url)
    # No Test model
    # Test UI
    context.ui_result = context.test.client.get(url)
    ui_result = context.ui_result.content.decode('utf-8')
    #print('context', ui_result)

@then(u'result has ic detail url for street "{street_slug}"')
def step_impl(context, street_slug):
    # No Test model
    url = reverse('bus_time:street_detail', args=[street_slug])
    # Test UI
    expecting = [
             'ic-get-from="{0}"'.format(url),
             'ic-trigger-on="load"',
            ]
    assert_contain(context, expecting, context.ui_result)


@when(u'intercooler uses url to get detail for a street "{street_slug}"')
def step_impl(context, street_slug):
    # Test model
    url = reverse('bus_time:street_detail', kwargs={'street_slug': street_slug})
    #print('url_detail', url)
    # No Test model
    # Test UI
    context.ui_result = context.test.client.get(url,
            data={'ic-request': 'true'})


@when(u'user searches for empty string')
def step_impl(context):
    # Test model
    # Test UI
    url = reverse('bus_time:search') + '?q='
    context.ui_result = context.test.client.get(url)


@then(u'result has div "{div_id}" "{message}"')
def step_impl(context, div_id, message):
    # No Test model
    # Test UI
    #print('context', context.ui_result.context['Street'])
    expecting= [
            '<div id="{0}"'.format(div_id),
            '>{0}<'.format(message),
            ]
    assert_contain(context, expecting, context.ui_result)


@when(u'user searches for "{search_text}"')
def search_for_street(context, search_text):
    # Test model
    #print('search_text:', search_text)
    context.model_result = busmdl.search(search_text)
    # street_name = 'street name'
    # Test UI
    #print('test for search')
    url = reverse('bus_time:search') + '?q=' + search_text
    #print('url:', url)
    context.ui_result = context.test.client.get(url)
    ui_result = context.ui_result.content.decode('utf-8')
    #print(ui_result)


@then(u'result has no "{model}"')
def step_impl(context, model):
    # Test model
    #print('street', context.model_result)
    assert len(context.model_result['BusRoute']) == 1
    # Test UI
    streets = context.ui_result.context[model]
    assert list(streets) == []


@then(u'result has "{model}" is "{bus_route}"')
def step_impl(context, model, bus_route):
    # Test model
    assert len(context.model_result['BusRoute']) == 1
    busroute = context.ui_result.context[model]
    #print('route', busroute)
    assert busroute[0].name == bus_route
    url = reverse('bus_time:route_detail', kwargs={'id_route' :
        busroute[0].external_id})
    # url = reverse('wip')
    # Test UI
    expecting = [
          'ic-get-from="{0}"'.format(url),
          'ic-target="#page_content">{0}'.format(bus_route)
          ]
    assert_contain(context, expecting, context.ui_result)

@then(u'result has "{model}" "{name_of_object}"')
def has_city_street(context, model, name_of_object):
    #print('model:', model)
    #print('name_of_object:', name_of_object)
    # Test model (unit test)
    # https://stackoverflow.com/a/4036665
    if model=='Street':
        city, name = name_of_object.split(' - ')
        query = busmdl.Street.objects.filter(city__name=city)
    else:
        query = busmdl.BusRoute.objects
        name = name_of_object
    needed_item = query.get(name=name)
    #print(needed_item)
    slug = slugify(unidecode(name_of_object))
    #print('slug:', slug)
    #print(context.model_result)
    # Get item that has model from context.model_result
    #found_items = []
    #found = [found['found'] for found in context.model_result
    #        if model == found['model']]
    #for name in found:
    #    found_items.extend([value for value in name])
    found_items = context.model_result[model]
    #print('found_items', found_items)
    # Verify that the item['found'] has street with name_of_object
    # and the street has correct slug
    #assert name_of_object in found
    list_name = [str(name) for name in found_items]
    #print(list_name)
    assert str(needed_item) in list_name
    #context.test.assertEqual(name_of_object, str(next(list_name)))
    # TODO
    # Test UI (action flow test)
    # This is not result of search
    #need_url = reverse(street, slug)
    #need_url = reverse('bus_time:streets') + slug
    # We have: model (Street / BusRoute), name_of_object ('Đà Nẵng - Hùng
    # Vương' / 'Xuân Diệu - Siêu thị Lotte - CĐ Việt Hàn')
    # We need: found_object (Street object with str = name_of_object /
    # BusRoute object with name = name_of_object)
    #klass = getattr(busmdl, model)
    #try:
    #    found_object = klass.objects.get(name=name_of_object)
    #except (klass.DoesNotExist):
    #    city_name, street_name = name_of_object.split(' - ', 1)
    #    found_object = klass.objects.get(city__name=city_name, name=street_name)
    #need_url = found_object.get_absolute_url() # If model is Street url has
    #expecting = [
    #        'ic-get-from="{0}"'.format(need_url),
    #        'ic-target="#page_content">{0}</button>'.format(name_of_object),
    #        ]
    #assert_contain(context, expecting, context.ui_result)


@when(u'user selects "{model}" "{street_name}"')
def select_street_name(context, model, street_name):
    #print('this_is_select', context.model_result)
    # Not Test model
    context.street_name = street_name
    # Only when complex task that need separate method
    # Test UI
    #found_items = []
    #found = [found['found'] for found in context.model_result]
    #for name in found:
    #    found_items.extend([value for value in name])
    url = reverse('bus_time:street_detail',
            kwargs={'street_slug': slugify(unidecode(street_name))})
    #print('url_detail', url)
    # No Test model
    # Test UI
    context.ui_result = context.test.client.get(url,
            data={'ic-request': 'true'})
    #print(context.ui_result.content.decode('utf-8'))


@then(u'result has main street "{street_slug}"')
def step_impl(context, street_slug):
    # Test model
    #print('ui_result.context', context.ui_result.context)
    context.street = busmdl.Street.objects.get(slug=street_slug)
    #assert context.ui_result.context['street'].slug == street_slug
    assert context.street.slug == street_slug
    # Test UI
    street_name = busmdl.Street.objects.get(slug=street_slug)
    url = reverse('bus_time:street_detail', kwargs={'street_slug': street_slug})
    city, street_name = str(street_name).split(' - ', 1)
    #print('url:', url)
    #context.ui_result = context.test.client.get(url)
    ui_result = context.ui_result.content.decode('utf-8')
    #print('context', ui_result)
    expecting = [
            '{0}</h1>'.format(street_name)
            ]
    assert_contain(context, expecting, context.ui_result)
    # TODO: show main street name without url


@then(u'result has "{side}" side bus stop "{bus_stop}" with routes "{bus_routes}"')
def has_bus_stop(context, side, bus_stop, bus_routes):
    # Teresult has "{side}" side bus stop "{bus_stop}" with routes
    # Test UI
    street = context.street_name
    external_id = busmdl.BusStop.objects.get(name=bus_stop).external_id
    url = reverse('bus_time:bus_stops', kwargs={'ext_id': external_id })
    # Split bus_routes then have urls
    ext_ids = [ext_id.strip() for ext_id in bus_routes.split(',')]
    url_routes = ['ic-get-from="{0}"'.format(
        reverse('bus_time:route_detail', kwargs={'id_route': bus_route}))
        # reverse('wip'))
        for bus_route in ext_ids]
    route_nums = ['ic-target="#page_content">{0}</button>'.format(
        bus_route.split('_')[-1])
        for bus_route in ext_ids]
    # TODO when have time: validate DNG_03 and DNG_11 belong to "Truoc 36"
    # Extend urls
    expecting = [
              'ic-get-from="{0}"'.format(url),
              'ic-target="#details">🚏{0}</button>'.format(bus_stop),
            ]
    expecting.extend(url_routes)
    expecting.extend(route_nums)
    assert_contain(context, expecting, context.ui_result)


@then(u'result has route "{route}" with "{street_type}" street "{seq}" is "{street_name}"')
def step_impl(context, route, street_type, seq, street_name):
    # Test model
    sors = context.ui_result.context[street_type + '_sor']
    #print('need_sor_list', need_sor_list)
    #need_sor = need_sor_list[int(sequence) - 1]
    #print(need_sor, need_sor.bus_route.external_id, '==', route)
    #assert route in [need_sor.bus_route.external_id for need_sor in
    #        need_sor_list]
    '''
    street_type | key
    ------------+-------------
    previous    | previous_sor
    first       | previous_sor
    next        | next_sor
    last        | next_sor
    street_type = 'previous'
    '''
    type_map = {
            'first': 'previous_sor',
            'last': 'next_sor',
            }
    #type_map = {
    #        'first': ('previous_sor', 2),
    #        'last': ('next_sor', 4),
    #        }
    try:
        key = type_map[street_type]
        idx = 2  # pragma: no cover 
        #key, idx = type_map[street_type]
    except (KeyError):
        key = street_type + '_sor'
        idx = 0
    # print('key_test', key)
    # need_sor_list = context.ui_result.context[key]
    # print('need_sor_list', need_sor_list)
    need_sor = sors[int(seq) - 1][int(idx)]
    # print('test_sor', need_sor.bus_stop.street.name,
    #        need_sor.bus_route.external_id, route)
    assert route == need_sor.bus_route.external_id
    #print('need_sor.bus_stop.street.name', need_sor.bus_stop.street.name)
    assert need_sor.bus_stop.street.name == street_name
    # Test UI

@then(u'result has "{count}" "{street_type}" stops on route')
def step_impl(context, count, street_type):
    #need_sors = context.street.get_sors_of_direction()
    #even_sors = bm.StopOnRoute.objects.filter(bus_stop__in=even_stops)
    #odd_sors = bm.StopOnRoute.objects.filter(bus_stop__in=odd_stops)
    # Test model
    #need_sor_list = context.street.get_other_street_sors(need_sors, -1)
    need_sor_list = context.ui_result.context[street_type + '_sor']
    #print('sor_need', need_sor_list)
    assert len(need_sor_list) == int(count)
    # No Test UI

@then(u'result has end div logic in stops on route')
def step_impl(context):
    # Test model
    end_div = context.ui_result.context['previous_sor']
    end_div = list(list(zip(*end_div))[1])
    # print('div', end_div)
    # TODO: have correct end_div data structure for each stop on route
    assert end_div == [
            True, # Route 03
            False, # Route 03
            True, # Route 11
            ]
    # No Test UI


# @when(u'user opens details url for a bus route "{id_route}"')
# def step_impl(context, id_route):
#     # Test model
#     # Test UI
#     url = reverse('bus_time:route_detail', kwargs={'id_route': id_route})
#     # print(url)
#     context.ui_result = context.test.client.get(url)
#     ui_result = context.ui_result.content.decode('utf-8')
#     # print('context', ui_result)
# 
# 
# @then(u'result has ic detail url for bus route "{id_route}"')
# def step_impl(context, id_route):
#     # No Test model
#     # url = reverse('bus_time:street_detail', args=[street_slug])
#     route = busmdl.BusRoute.objects.get(external_id=id_route)
#     # Test UI
#     expecting = [
#              '{0}</h1>'.format(route)
#             ]
#     assert_contain(context, expecting, context.ui_result)



@when(u'user selects a bus route "{id_route}"')
def step_impl(context, id_route):
    url = reverse('bus_time:route_detail', kwargs={'id_route': id_route})
    # No Test model
    context.bus_route = busmdl.BusRoute.objects.get(external_id=id_route)
    # Test UI
    context.ui_result = context.test.client.get(url,
            data={'ic-request': 'true'})
    #ui_result = context.ui_result.content.decode('utf-8')
    #print('context', ui_result)
    #count_return_stops_of_streets = context.ui_result.context['forward_stops']


@then(u'result has "{amount}" bus stops of "{forward}" street')
def step_impl(context, amount, forward):
    # Test model
    stops = context.ui_result.context[forward + '_sors']
    # Test UI
    # print(len(stops))
    assert len(stops) == int(amount)


@then(u'result has "{count_buses}" buses not reached bus stop with direction "{direction}"')
def step_impl(context, count_buses, direction):
    direction_choice = {
        'forward': busmdl.FORWARD,
        'return': busmdl.RETURN,
        }
    # Test model
    # print('bus_route', context.bus_route)
    at_time = context.test.client.session.get('at_time') or context.time_now
    buses = context.ui_result.context[direction + '_buses']
    sobs = context.ui_result.context[direction + '_sobs']
    sors = context.bus_route.stoponroute_set.filter(
            direction=direction_choice[direction])
    last_sobs = context.bus_route.get_last_sobs(
            at_time, direction_choice[direction], sobs,  buses)
    result = buses.filter(stopofbus__in=last_sobs)
    # print('count_result', result.count())
    assert int(count_buses) == result.count()
    # Test UI


@when(u'user selects at time "{at_time}" at home page')
def step_impl(context, at_time):
    # Test model
    # Test UI
    url = reverse('bus_time:set_tz')
    context.ui_result = context.test.client.get(url,
            data={'ic-request': 'true', 'at_time': at_time})


@then(u'result has at time "{at_time}" at home page')
def step_impl(context, at_time):
    # Test model
    assert str(context.test.client.session.get('at_time')) == at_time
    # Test UI


@when(u'user selects at time "{at_time}" with bus route "{id_route}"')
def step_impl(context, at_time, id_route):
    # Test model
    # Test UI
    context.bus_route = busmdl.BusRoute.objects.get(external_id=id_route)
    url = reverse('bus_time:set_tz')
    url_busroute = reverse('bus_time:route_detail',
            kwargs={'id_route': id_route})
    context.result = context.test.client.get(url,
            data={'ic-request': 'true', 'at_time': at_time,
                'object_with_id': 'bus_route-' + id_route})
    context.ui_result = context.test.client.get(url_busroute,
            data={'ic-request': 'true', 'at_time': at_time,
                'object_with_id': 'bus_route-' + id_route})


#@then(u'result has "{bus}"with "arrived_time" of "direction"')
#def step_impl(context, bus, arrived_time, direction):
#    # Test model
#

@then(u'result has rowspan counts for bus stops of streets in bus route')
def step_impl(context):
    # Test model
    # Test UI
    rowspan_forward_view = context.ui_result.context['forward_sors']
    rowspan_forward_view = list(zip(*rowspan_forward_view))
    rowspan_return_view = context.ui_result.context['return_sors']
    rowspan_return_view = list(zip(*rowspan_return_view))
    rowspan_forward = [1, 1, 2, 2, 3, 3, 3, 3, 3, 3, 1, 2, 2, 1]
    rowspan_return = [1, 2, 2, 1, 3, 3, 3, 2, 2, 2, 2, 1, 1]
    assert rowspan_forward == list(rowspan_forward_view[1])
    assert rowspan_return == list(rowspan_return_view[1])


@when(u'user selects "{model}" has name "{bus_route_name}" has id "{ext_id}"')
def step_impl(context, model, bus_route_name, ext_id):
    # Test model
    context.ext_id = ext_id
    context.route_name = bus_route_name
    url = reverse('bus_time:route_detail', kwargs={'id_route': ext_id})
    context.ui_result = context.test.client.get(url,
            data={'ic-request': 'true'})
    # Test UI
    #print(context.ui_result.content.decode('utf-8'))


@then(u'result has main bus route "{id_route}"')
def step_impl(context, id_route):
    bus_route = busmdl.BusRoute.objects.get(external_id=id_route)
    #assert context.ui_result.context['bus_route'].external_id == id_route
    assert bus_route.external_id == id_route
    # Test UI
    #context.street_name = busmdl.Street.objects.get(slug=street_slug)
    url = reverse('bus_time:route_detail', kwargs={'id_route': id_route})
    # url = reverse('wip')
    bus_route_name = context.route_name
    #print('url:', url)
    #context.ui_result = context.test.client.get(url)
    #ui_result = context.ui_result.content.decode('utf-8')
    #print('context', ui_result)
    expecting = [
            '{0}</h1>'.format(bus_route_name)
            ]
    assert_contain(context, expecting, context.ui_result)
    # Test model
    # Test UI


@then(u'result has "{direction}" street and bus stop of street')
def step_impl(context, direction):
    # Test model
    sors = context.ui_result.context[direction + '_sors']
    url_street = ['ic-get-from="{0}"'.format(
        reverse('bus_time:street_detail', kwargs={'street_slug':
            sor[0].bus_stop.street.slug}))
        for sor in sors]
    #print('id', [stop[0].external_id for stop in busstops])
    url_stop = ['ic-get-from="{0}"'.format(
            reverse('bus_time:bus_stops',
            kwargs={'ext_id': sor[0].bus_stop.external_id}))
            for sor in sors]
    name_street = ['ic-target="#page_content">{0}'.format(
        sor[0].bus_stop.street.name) for sor in sors]

    name_stop = ['ic-target="#details">{0}'.format(
        sor[0].bus_stop.name) for sor in sors]

    expecting = [
              '{0}</h1>'.format(context.route_name),
              ]
    expecting.extend(url_street)
    expecting.extend(name_street)
    expecting.extend(url_street)
    expecting.extend(name_street)
    # Test UI
    #assert False
    assert_contain(context, expecting, context.ui_result)


@when(u'user opens detail url for a bus stop "{ext_id}"')
def step_impl(context, ext_id):
    url = reverse('bus_time:bus_stops', kwargs={'ext_id': ext_id})
    # No Test model
    # Test UI
    context.ui_result = context.test.client.get(url)
    #ui_result = context.ui_result.content.decode('utf-8')
    #print('context', ui_result)


@then(u'result has ic detail url for bus stop "{ext_id}"')
def step_impl(context, ext_id):
    # No Test model
    url = reverse('bus_time:bus_stops', args=[ext_id])
    # Test UI
    expecting = [
             'ic-get-from="{0}"'.format(url),
             'ic-trigger-on="load"',
            ]
    assert_contain(context, expecting, context.ui_result)

@when(u'current time is "{time_now}"')
def step_impl(context, time_now):
    # Test model
    # Test UI
    import pytz
    from bus_time.views import test_data
    from datetime import datetime
    test_data['test_time'] = time_now
    now = timezone.now()  # to have date
    DT_FORMAT = '%Y-%m-%d %H:%M %Z'
    result = now.strftime('%Y-%m-%d {0} %Z').format(time_now)
    at_time = datetime.strptime(result, DT_FORMAT)
    context.time_now = at_time.astimezone(pytz.UTC)


@when(u'cache stat is all zeroes')
def step_impl(context):
    # Test model
    busmdl.cache_stat['cache_missed'] = 0
    busmdl.cache_stat['cache_hit'] = 0
    # Not Test UI


@when(u'user selects bus stop "{bus_stop}"')
def step_impl(context, bus_stop):
    # Test model
    context.bus_stop = busmdl.BusStop.objects.get(name=bus_stop)
    ext_id = context.bus_stop.external_id
    #print('ext_id', ext_id)
    context.url = reverse('bus_time:bus_stops', kwargs={'ext_id': ext_id})
    url = context.url
    # Test UI
    context.ui_result = context.test.client.get(url,
            data={'ic-request': 'true'})
    #ui_result = context.ui_result.content.decode('utf-8')
    #print('context', ui_result)


@then(u'cache missed is "{missed_num}" and cache hit is "{hit_num}"')
def step_impl(context, missed_num, hit_num):
    # Test model
    #print(busmdl.cache_stat['cache_missed'], busmdl.cache_stat['cache_hit'])
    # print('cache_stat', busmdl.cache_stat)
    assert busmdl.cache_stat['cache_missed'] == int(missed_num)
    assert busmdl.cache_stat['cache_hit'] == int(hit_num)


@then(u'result has bus stop and street "{street}"')
def step_impl(context, street):
    # Test model: str method
    stop = context.bus_stop
    expecting = '{0}: {1} {2} {3}'.format(
            stop.pk, stop.street, 'Even' if stop.even else 'Odd', stop.name)
    assert str(stop) == expecting
    # Test UI
    assert street == stop.street.name
    street_slug = busmdl.Street.objects.get(name=street).slug
    url_street = reverse('bus_time:street_detail', kwargs={'street_slug': street_slug})
    bus_stop = context.bus_stop.name
    ui_result = context.ui_result.content.decode('utf-8')
    #print('context', ui_result)
    expecting = [
            '{0}</h3>'.format(bus_stop),
            'ic-get-from="{0}"'.format(url_street),
            'ic-target="#page_content">{0}</button>'.format(street),
            ]
    assert_contain(context, expecting, context.ui_result)



@then(u'result has route "{sequence}" "{route_id}" with direction "{direction}"')
def step_impl(context, sequence, route_id, direction):
    # Test model
    route_sor = context.ui_result.context['route_sor']
    #print('sor_route', route_sor)
    route = route_sor[int(sequence)- 1][0]
    sor =  route_sor[int(sequence) - 1][1]
    assert route_id == route.external_id
    assert direction == sor.direction
    en_direc = dict(busmdl.DIRECTION)[direction]
    #print('en_direc', en_direc)
    # from direction can we have en_direc?
    # or from en_direc can we have direction?
    # Test UI
    url_route = reverse('bus_time:route_detail', kwargs={'id_route': route_id})
    # url_route = reverse('wip')
    # ui_result = context.ui_result.content.decode('utf-8')
    # print('context', ui_result)
    expecting = [
            'ic-get-from="{0}"'.format(url_route),
            'ic-target="#page_content">{0}</button>'.format(route_id),
            '{0}</strong>'.format(en_direc),
            ]
    assert_contain(context, expecting, context.ui_result)


@then(u'result has bus "{bus_code}" with estimated time "{eta}"')
def step_impl(context, bus_code, eta):
    # Test model
    from datetime import datetime, date, timedelta # datetime: class
    #bus_sob = context.ui_result.context['bus_estm']
    #print('time_now', context.time_now)
    #time_now = time.strptime(context.time_now, 
    bus_sob = context.bus_stop.get_routes_with_buses_and_eta(context.time_now, 5)
    #print('bus_sob', *bus_sob, sep='\n')
    time_est = [eta.strftime("%H:%M") for bus, eta in bus_sob]
    assert eta in time_est
    # TODO Next: fix this test to include route
    #print(len(bus_code))
    #print(bus_sob[0][0].external_id[:-len(bus_code)])
    #print(bus_sob[0][0].external_id[-len(bus_code):])
    bus = [bus for bus, eta in bus_sob
            if bus.external_id[-len(bus_code):] == bus_code][0]
    expecting = '{0}: {1} {2}'.format(bus.pk, bus.external_id,
            bus.bus_route.name)
    # Nguyen tac: doi qua dang trung gian don gian: so nguyen
    # Bai toan: tu 9:17 doi thanh 2020-08-19 9:17
    # su dung timedelta anh huong gi ko a?
    #print('xe', bus_code, 'tg', eta)
    # Test UI
    ui_result = context.ui_result.content.decode('utf-8')
    # print('context', ui_result)
    expecting = [
            '🚍&nbsp;{0}:&nbsp;<strong>⌛&nbsp;{1}<'.format(bus_code, eta),
            ]
    assert_contain(context, expecting, context.ui_result)

#@when(u'user needs time table with "{input_name}" as "{value}"')
#def input_value(context, input_name, value):
#    assert False
#
#
#@when(u'user selects bus stop "{bus_stop}"')
#def select_bus_stop(context, bus_stop):
#    assert False
#
#
#@then(u'result has "{bus_direction}" with estimated arrival times "{est_times}"')
#def has_direction_and_times(context, bus_direction, est_times):
#    assert False


@when(u'user selects time zone "{timezone_name}" with street slug "{street_slug}"')
def step_impl(context, street_slug, timezone_name):
    # Test model
    # Test UI
    # print('session_at_time', context.test.client.session.get('at_time'))
    url = reverse('bus_time:set_tz')
    if timezone_name == "''":
        timezone_name = ''
    context.ui_result = context.test.client.get(url,
            data={'ic-request': 'true', 'timezone': timezone_name,
                'object_with_id': 'street-' + street_slug})
    # ui_result = context.ui_result.content.decode('utf-8')
    # print('context', ui_result)


@then(u'result has time zone "{timezone_name}"')
def step_impl(context, timezone_name):
    # Test model
    # Test UI
    expecting = context.test.client.session.get('django_timezone')
    # print('name_tz', expecting)
    if timezone_name == "None":
         timezone_name = None
    assert expecting == timezone_name


@then(u'result has at time "{at_time}"')
def step_impl(context, at_time):
    # Test model
    # Test UI
    time = context.test.client.session.get('at_time')
    print('time', time)
    assert str(time) == at_time
