import datetime
import hashlib

from behave import given, when, then
from django.conf import settings
from django.shortcuts import reverse
#from django.template.loader import render_to_string
from django.test import TestCase, Client
# https://stackoverflow.com/questions/8287883/django-how-to-get-format-date-in-views
from django.utils import timezone, formats
from request_token.models import RequestToken
from request_token.settings import JWT_SESSION_TOKEN_EXPIRY

from hash_email_auth.models import User, TOKEN_SCOPE
from hash_email_auth.forms import EmailAuthenticationForm
from hash_email_auth.views import get_home_url
from main.tests import get_client as main_get_client


def assert_contain(context, expecting, response):
    '''
    Decode content of client, then check if decoded content has strings in
    expecting list.
    '''
    ui_result = response.content.decode('utf-8')
    #print('ui_result', ui_result))
    result = [
                need_test for need_test in expecting
                if need_test in ui_result
            ]
    context.test.assertEqual(expecting, result)


def get_client(context, device, email=None):
    client = main_get_client(context, device)
    if email:
        client.hashed_login_email = check_hashed_email(context, email)
    else: pass
    return client


def check_hashed_email(context, email):
    hashed_email = User.objects.hash_email(email)
    expecting = 'sha3_512.' + hashlib.sha3_512(bytes(email, 'utf-8')
            ).hexdigest()
    #print(hashed_email, expecting)
    context.test.assertEqual(expecting, hashed_email)
    return hashed_email


@given(u'user has no account with "{email}" on system')
def no_user_account(context, email):
    hashed_email = check_hashed_email(context, email)
    #User.objects.create_user(email)
    context.test.assertRaises(User.DoesNotExist, User.objects.get,
            hashed_email=hashed_email)


@given(u'user with "{email}" has not logged in to system from device "{device}"')
def step_impl(context, email, device):
    client = get_client(context, device)
    #user = User.objects.create_user(username='username', password='test')
    context.test.assertFalse('_auth_user_id' in client.session)
    #auth_user_id = client.session['_auth_user_id']
    #print('user after:', auth_user_id)


@when(u'user opens home page from device "{device}"')
def step_impl(context, device):
    client = get_client(context, device)
    client.response = client.get(get_home_url(), follow=True)


@then(u'the page at device "{device}" has intercooler link to get authentication')
def step_impl(context, device):
    # Not Test model
    # Test UI
    #print('device:', device)
    client = get_client(context, device)
    url = reverse('hash_email_auth:status')
    expecting = [
            'ic-get-from="{0}" ic-trigger-on="load"'.format(url),
            'id="auth_info"',
            'id="user_info"',
            ]
    #content = client.response.content
    #print(content.decode('utf-8'), '\n', expecting)
    assert_contain(context, expecting, client.response)


@when(u'user at device "{device}" click logout link')
def step_impl(context, device):
    # No Test model
    # Test UI
    client = get_client(context, device)
    client.response = client.get(reverse('hash_email_auth:logout'),
            follow=False)


@then(u'the page at device "{device}" has redirect instruction for ic')
def step_impl(context, device):
    # No Test model
    # Test UI
    client = get_client(context, device)
    url = get_home_url()
    assert client.response['X-IC-Redirect'] == url


@when(u'intercooler at device "{device}" open link to get authentication')
def step_impl(context, device):
    # Not Test model
    # Test UI
    client = get_client(context, device)
    url = reverse('hash_email_auth:status')
    client.response = client.get(url, follow=True)


@then(u'the page at device "{device}" has login form')
def step_impl(context, device):
    client = get_client(context, device)
    #print(client.response.content.decode('utf-8'))
    url = reverse('hash_email_auth:login')
    tnc_url = reverse('terms_and_conds')
    expecting = [
            '>Login<',
            'ic-post-to="{0}" ic-target="#auth_info"'.format(url),
            'name="login_email"',
            '<input type="submit" value="Login"',
            '>Terms And Conditions<',
            'ic-get-from="{0}" ic-target="#page_content"'.format(tnc_url),
            ]
    #content = client.response.content
    #print(content.decode('utf-8'), '\n', expecting)
    assert_contain(context, expecting, client.response)


@then(u'the page at device "{device}" has account info of "{email}"')
def step_impl(context, device, email):
    client = get_client(context, device)
    user = User.objects.get(email=email)
    assert user.email == None
    content = client.response.content.decode('utf-8')
    #print(content)
    url = reverse('hash_email_auth:logout')
    tnc_url = reverse('terms_and_conds')
    expecting = [
            'ic-get-from="{0}" ic-target="#user_info"'.format(url),
            '>{0}<'.format(user.username),
            '>Terms And Conditions<',
            'ic-get-from="{0}" ic-target="#page_content"'.format(tnc_url),
            ]
    assert_contain(context, expecting, client.response)


@when(u'user provides "{email}" email in login form at device "{device}"')
def step_impl(context, email, device):
    client = get_client(context, device, email)
    client.login_email = email
    resp_context = client.response.context
    #print('Response context:', resp_context)
    data = {
            'csrf_token': str(resp_context['csrf_token']),
            'login_email': email,
            }
    #print(data)
    url = reverse('hash_email_auth:login')
    client.response = client.post(url, data, follow=True)


@then(u'response at device "{device}" has incorrect email message')
def step_impl(context, device):
    # Not Test model
    # Test UI
    client = get_client(context, device)
    content = client.response.content.decode('utf-8')
    expecting = 'Please provide valid email address.'
    #print(content, '\n', expecting)
    context.test.assertContains(client.response, expecting, status_code=400)


@then(u'response at device "{device}" has email sent message')
def step_impl(context, device):
    # Test model
    # Test UI
    client = get_client(context, device)
    content = client.response.content.decode('utf-8')
    expecting = 'Please check email to continue loging in.'
    #print(content, '\n', expecting)
    context.test.assertContains(client.response, expecting)


@then(u'user with "{email}" has "{count}" login token')
def step_impl(context, email, count):
    # Test model
    hashed_email = check_hashed_email(context, email)
    user = User.objects.get(hashed_email=hashed_email)
    assert user.request_tokens.filter(used_to_date=0).count() == int(count)
    # No Test UI


@then(u'user received email with one-time link for device "{device}"')
def step_impl(context, device):
    # Test model
    client = get_client(context, device)
    hashed_email = client.hashed_login_email
    user = User.objects.get(hashed_email=hashed_email)
    #print('token count:', user.request_tokens.filter(used_to_date=0).count())
    assert not user.is_active
    #token = RequestToken.objects.get(scope="login." + hashed_email)
    token = user.get_login_token()
    #print(token.expiration_time)
    assert token.expiration_time > timezone.now()
    #token2 = RequestToken.objects.get(token=token.jwt())
    # Test UI
    server_host = 'testserver'
    url = 'https://' + server_host + reverse(TOKEN_SCOPE) + '?rt=' + token.jwt()
    email_form = EmailAuthenticationForm(
            data={'login_email': client.login_email})
    assert email_form.is_valid()
    emsg = email_form.send_email(server_host, token, is_testing=True)
    #print('message', emsg.body, '\n', emsg.alternatives)
    #print(url)
    message_html = emsg.alternatives[0][0]
    assert url in emsg.body
    assert url in message_html
    expr_time = formats.date_format(token.expiration_time,
            settings.DATETIME_FORMAT)
    assert expr_time in emsg.body
    assert expr_time in message_html


@given(u'user with "{email}" has expired login token')
def step_impl(context, email):
    # Test model
    user = User.objects.pre_authenticate(email)
    token = user.get_login_token()
    token.expiration_time -= datetime.timedelta(
            minutes=JWT_SESSION_TOKEN_EXPIRY + 1)
    token.save()
    #print(token.expiration_time)
    # No Test UI


@when(u'user clicks one-time link in login email on device "{device}"')
def step_impl(context, device):
    client = get_client(context, device)
    hashed_email = client.hashed_login_email
    user = User.objects.get(hashed_email=hashed_email)
    token = user.get_login_token()
    url = reverse(TOKEN_SCOPE) + '?rt=' + token.jwt()
    #print('login_url:', url)
    client.response = client.get(url, follow=False)


@then(u'there is active account with hashed "{email}" on system')
def step_impl(context, email):
    # Test model
    hashed_email = check_hashed_email(context, email)
    user = User.objects.get(hashed_email=hashed_email)
    assert user.request_tokens.filter(used_to_date=0).count() == 0
    assert user.is_active
    # Not Test UI


@then(u'user is logged in device "{device}" with account for email "{email}"')
def step_impl(context, email, device):
    # Not Test model
    # Test UI
    client = get_client(context, device)
    hashed_email = check_hashed_email(context, email)
    user = User.objects.get(hashed_email=hashed_email)
    context.test.assertTrue('_auth_user_id' in client.session)
    context.test.assertEqual(str(user.id), client.session['_auth_user_id'])


@then(u'response in device "{device}" has redirect link')
def step_impl(context, device):
    # Not Test model
    # Test UI
    client = get_client(context, device)
    #print('response content', client.response.content.decode('utf-8'))
    assert client.response.status_code == 307


@then(u'user is not logged in device "{device}" with account for email "{email}"')
def step_impl(context, email, device):
    # Not Test model
    # Test UI
    client = get_client(context, device)
    context.test.assertFalse('_auth_user_id' in client.session)


@given(u'user has account with hashed "{email}" on system')
def step_impl(context, email):
    user = User.objects.pre_authenticate(email=email)
    assert user.email == None
    user.activate()


@given(u'user with "{email}" has logged in to system from device "{device}"')
def step_impl(context, email, device):
    user = User.objects.get(email=email)
    client = get_client(context, device)
    client.force_login(user=user)


@then(u'there is no new account created')
def step_impl(context):
    assert User.objects.count() == 1


@when(u'create user with username "{username}", email "{email}", password "{password}"')
def step_impl(context, username, email, password):
    username = None if username == 'None' else username
    email = None if email == 'None' else email
    password = None if password == 'None' else password
    # Test model
    try:
        user = User.objects.create_user(username, email, password=password)
        context.exception = None
    except Exception as exception:
        context.exception = exception
    # Not Test UI


@then(u'"{num_user}" user is created with username "{username}" "{email_state}" email "{passwd_state}" password')
def step_impl(context, num_user, username, email_state, passwd_state):
    # Test model
    if num_user == 'no':
        assert context.exception != None
        return
    else: #if num_user == 'one':
        user = User.objects.get(username=username)
    context.test.assertNotEqual(None, user.hashed_email)
    if email_state == 'with':
        context.test.assertNotEqual(None, user.email)
    else:
        context.test.assertEqual(None, user.email)
    if passwd_state == 'with':
        context.test.assertNotEqual('', user.password)
    else:
        context.test.assertEqual('', user.password)
    # Not Test UI
