import pytz

from collections import OrderedDict
from datetime import datetime, timedelta # datetime: class

# https://stackoverflow.com/questions/4631865/caching-query-results-in-django
from django.core.cache import cache
from django.db import models
from django.conf import settings
#from django.urls import reverse
from django.utils import timezone
#from django.utils.text import slugify
from django.utils.translation import gettext as _
#from unidecode import unidecode

from cities_light.models import City


FORWARD = 'Di' # FW
RETURN = 'Ve' # BW
DIRECTION = (
    (FORWARD, _('Forward')),
    (RETURN, _('Return')),
)


def make_cache_key(sor, time):
    time = str(time).replace(" ", "-")
    key = 'sor_{0}_{1}'.format(str(sor.pk), time)
    return key

def calc_eta(time, minutes):
    now = timezone.now()
    return datetime.combine(now, time, now.tzinfo) + timedelta(minutes=minutes)


def search(for_text):
    '''
    Search for_text in applicable models then return list of matched objects.

    # This is just one of possible solutions. Please propose another one.
    return [
            {
                'model': 'Street',
                'found': ['city - street name', 'city - street name'...]
                }...
            ]
    '''
    if for_text.strip() == "":
        streets = None
        routes = None
        message = 'Cần nhập tiêu chí tìm kiếm khác rỗng'
    #print('BusRoute:', BusRoute.objects.all())
    else:
        streets = Street.objects.filter(name__contains=for_text.strip())
        busstops = BusStop.objects.filter(street__in=streets)
        routes = BusRoute.objects.filter(models.Q(bus_stop__in=busstops)
                | models.Q(name__contains=for_text.strip())).distinct()
        #print(routes.query)
        message = None
        if routes.count() == 0:
            message = 'Không tìm thấy kết quả'
        else: pass
        #print(routes)
        #print(routes.query)
    return OrderedDict({
            'Street': streets,
            'BusRoute': routes,
            'message': message
            })


class StrPKAbstractModel(models.Model):
    class Meta:
        abstract = True

    def __str__(self):
        return '{0}: {1}'.format(self.pk, self.describe())


class NameAbstractModel(StrPKAbstractModel):
    name = models.CharField('Name', max_length=200)

    class Meta:
        abstract = True

    def describe(self):
        return self.name


class ExternalIdModel(StrPKAbstractModel):
    external_id = models.CharField(max_length=20, blank=True, null=True,
            unique=True)

    class Meta:
        abstract = True

    def get_second_id(self):
        text, num = self.external_id.split('_')
        return num

    def get_third_id(self):
        text, text2, num = self.external_id.split('_', 2)
        return num

    def describe(self): # pragma: no cover
        return self.external_id


class Passenger(models.Model):
    full_name = models.CharField('Full name', max_length=200)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name='User',
            on_delete=models.CASCADE,)
    balance = models.FloatField('Balance', default=0)

    class Meta:
        verbose_name = 'Passenger'
        verbose_name_plural = 'Passengers'


class Street(NameAbstractModel, ExternalIdModel):
    ''' each city has list of streets'''
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    #slug: city name+street name, unique,
    # TODO: change when street name or city change
    slug = models.SlugField(max_length=200)

    class Meta:
        verbose_name = 'Street'
        verbose_name_plural = 'Streets'

    def __str__(self):
        return self.city.name + ' - ' + self.name

    def get_bus_stops(self, is_even):
        ''' Return list of bus stops on routes filtered by side. '''
        stops = self.busstop_set.filter(even=is_even)
        return stops

    #def get_sequenced_streets(self):
    #    '''
    #    Return list of bus stops on previous and next streets group by route.
    #    '''

    #def get_sors_of_direction(self, direction):
    #    stops = self.busstop_set.all()
    #    sors = StopOnRoute.objects.filter(
    #            bus_stop__in=stops).order_by('bus_route')
    #    # TODO: check which scenario that has different directions
    #    sors_direction = sors.filter(direction=direction)
    #    return sors_direction

    def get_sors_on_other_street(self, list_sors, offset=0,
            first=False, last=False):
        sor_pre = [sor.get_stop(offset, first=first, last=last)
                for sor in list_sors]
                #for sor in list_sors
                # if sor is not null
        sors = [sor for sor in sor_pre if sor and sor.bus_stop.street != self]
        return sor_pre if first or last else sors


class Transaction(NameAbstractModel):
    passenger = models.ForeignKey(Passenger, verbose_name='Passenger',
            on_delete=models.CASCADE)
    date = models.DateField('Date')
    category = models.CharField('Category', max_length=200)
    amount = models.FloatField('Amount')

    class Meta:
        verbose_name = 'Transaction'
        verbose_name_plural = 'Transactions'


class Duration(ExternalIdModel):
    start_time = models.TimeField('Start time')
    minutes = models.IntegerField('Minutes')

    class Meta:
        verbose_name = 'Duration'
        verbose_name_plural = 'Durations'


cache_stat = {
        'cache_hit': 0,
        'cache_missed': 0,
        }

class BusStop(NameAbstractModel, ExternalIdModel):
    ''' each street has list of bus stops'''
    street = models.ForeignKey(Street, verbose_name='Street',
            on_delete=models.CASCADE)
    even = models.BooleanField('Even')
    order = models.IntegerField('Order', blank=True, null=True)
    latitude = models.FloatField('Latitude', default=0)
    longitute = models.FloatField('Longitute', default=0)

    class Meta:
        verbose_name = 'Bus Stop'
        verbose_name_plural = 'Bus Stops'

    def get_routes_with_buses_and_eta(self, at_time, num_list):
        # print('at_time_models', at_time)
        list_buses_estm = []
        for sor in self.stoponroute_set.all():
            buses_of_sor = []
            cached_buses = cache.get(make_cache_key(sor, at_time))
            if cached_buses:
                #print('4. Exist, Get from cache 1 bus with 5 eta:')
                cache_stat['cache_hit'] += 1
                #print('cache_hit', cache_stat['cache_hit'])
                list_buses_estm.extend(cached_buses[:num_list])
                continue
            else: pass

            #print('1. Not exist: from sor, calculate 1 bus with 5 eta: done')
            cache_stat['cache_missed'] += 1
            buses_not_reached_bus_stop = self.get_buses_not_reached_bus_stop(
                    at_time, sor)
            #print('buses_not_reached_bus_stop', buses_not_reached_bus_stop)
            num_buses = 5 - len(buses_not_reached_bus_stop)
            sum_minute = sor.get_sum_all_average_time()
            buses_not_start = Bus.get_buses_not_start(at_time, sum_minute,
                        sor, num_buses)
            #print('sum_time_view',  sor.bus_route.pk, sor.direction, sum_minute)
            buses_of_sor.extend(buses_not_reached_bus_stop)
            buses_of_sor.extend(buses_not_start)
            list_buses_estm.extend(buses_of_sor[:num_list])
            #print('2. Saved 1 bus with 5 eta to cache:')
            eta_of_bus_before_first_bus = at_time #'WIP'
            cached_buses = cache.set(make_cache_key(sor,
                eta_of_bus_before_first_bus), buses_of_sor)
        #print('cache_missed',  cache_stat['cache_missed'])
        return list_buses_estm

    def get_buses_not_reached_bus_stop(self, at_time, sor):
        sob_not_in_sob_of_bus_stop = self.stopofbus_set.filter(
                bus__direction=sor.direction,
                arrived_time__lt=at_time, bus__bus_route=sor.bus_route)

        # Option 1
        arrived_sobs = StopOfBus.objects.filter(arrived_time__lt=at_time,
                bus__bus_route=sor.bus_route, bus__direction=sor.direction)
        buses_in_sob_arrived = Bus.objects.filter(
                stopofbus__in=arrived_sobs).distinct()
        #print('buses_sob_arr', buses_in_sob_arrived)
        list_buses = buses_in_sob_arrived.exclude(
                stopofbus__in=sob_not_in_sob_of_bus_stop)

        # Option 2
        #self_arrived_sobs = self.stopofbus_set.filter(arrived_time__lt=at_time,
        #        bus__bus_route=bus_route, bus__direction=direction)
        #arrived_buses = [sob.bus for sob in self_arrived_sobs]
        #arrived_sobs_before_self = StopOfBus.objects.filter(
        #        arrived_time__lt=at_time, bus__in=bus_route.bus_set.all(),
        #        bus__bus_route=bus_route, bus__direction=direction,
        #        ).exclude(bus__in=arrived_buses)
        #new_buses = Bus.objects.filter(pk__in=set(sob.bus.pk
        #        for sob in arrived_sobs_before_self))
        #print('get_buses_not_reached_bus_stop', #arrived_buses,
        #        #arrived_sobs_before_self,
        #        new_buses, sep='\n')
        buses = [(bus, sor.get_estimated_time(bus, at_time))
                for bus in list_buses
                if sor.get_estimated_time(bus, at_time) >= at_time]
        #print('buses', buses)
        return buses

    #def long_name(self):
    #    return '{0} ({1}) {2}'.format(
    #            self.street, 'Even' if self.even else 'Odd', self.name)

    def describe(self):
        return '{0} {1} {2}'.format(
                self.street, 'Even' if self.even else 'Odd', self.name)


class BusCompany(NameAbstractModel, ExternalIdModel):
    ''' Each city has list of operating bus companies'''
    phone_number = models.CharField('Phone number', max_length=200)

    class Meta:
        verbose_name = 'Bus Company'
        verbose_name_plural = 'Bus Companies'


class BusRoute(NameAbstractModel, ExternalIdModel):
    ''' Each bus company has list of bus routes. '''
    operators = models.ManyToManyField(BusCompany, verbose_name='Operator')
    bus_stop = models.ManyToManyField(BusStop, verbose_name='Bus stop',
            through='StopOnRoute', through_fields=('bus_route', 'bus_stop'))
    frequency = models.ManyToManyField(Duration, verbose_name='Frequency')
    price = models.IntegerField('Price')

    class Meta:
        verbose_name = 'Bus Route'
        verbose_name_plural = 'Bus Routes'

    def get_running_bus_eta(self, at_time, sors, sobs, direction, buses):
        ''' Stoponroute and estimated time. '''
        result = []
        last_sobs = self.get_last_sobs(at_time, direction, sobs, buses)
        buses_started = buses.filter(stopofbus__in=last_sobs)
        bus_stop_direction = self.bus_stop.filter(stoponroute__direction=direction)

        for bus in buses_started:
            bus_stop_not_reached = bus_stop_direction.exclude(
                            stopofbus__in=sobs.filter(bus=bus)).distinct()
            sors_not_reached = sors.filter(bus_stop__in=bus_stop_not_reached)
            eta_list = [(sor, sor.get_estimated_time(bus, at_time))
                    for sor in sors_not_reached]
            result.extend(eta_list)
        return result

    def calculate_eta_table(self, at_time, sors, sobs, direction, buses):
        # get the buses has not start
        buses_not_start = buses.exclude(stopofbus__in=sobs)
        # calculator estimated time for the buses not start
        eta_list_buses_not_start = [(sor, calc_eta(bus.start_time,
                sor.get_sum_all_average_time()))
                for sor in sors for bus in buses_not_start]
        # calculator estimated time for buses,
        # but buses has not arrived yet last bus stop
        eta_list = self.get_running_bus_eta(at_time, sors, sobs, direction,
                buses)
        eta_list.extend(eta_list_buses_not_start)
        return eta_list

    def get_last_sobs(self, at_time, direction, direction_sobs, buses):
        ''' Filter last stopofbuses of route. '''
        # get last stoponroute in route
        last_sor = self.stoponroute_set.filter(direction=direction).last()
        # filter last stopofbuses
        last_sobs = [direction_sobs.filter(bus=bus).last() for bus in buses]
        last_sobs = [sob for sob in last_sobs if sob]
        return last_sobs


class StopOnRoute(models.Model):
    bus_stop = models.ForeignKey(BusStop, verbose_name='Bus stop',
            on_delete=models.CASCADE)
    direction = models.CharField(max_length=20, verbose_name='Direction',
            choices=DIRECTION)
    bus_route = models.ForeignKey(BusRoute, verbose_name='Bus route',
            on_delete=models.CASCADE)
    order = models.IntegerField('Order')
    average_time = models.IntegerField('Average time')

    class Meta:
        verbose_name = 'Stop on Route'
        verbose_name_plural = 'Stops on Route'
        ordering = ['direction', 'order']

    def get_sum_all_average_time(self):
        sors = StopOnRoute.objects.filter(direction=self.direction,
                bus_route=self.bus_route, order__lte=self.order)
        sum_minute = sors.aggregate(models.Sum('average_time'))
        return sum_minute['average_time__sum']

    def get_estimated_time(self, bus, at_time):
        '''
        Calculate estimated time of arrival (ETA) by getting arrived_time from
        StopOfBus of `bus` from BusStop from previous StopOnRoute of self. This
        can be used only for single calculation and to clarify the logic.
        '''
        # select * from (select row_number, bus.id, sob.id from bus inner join sob...
        # order by ... desc)
        # where row_number = 1
        sob = StopOfBus.objects.filter(bus=bus,
                arrived_time__lt=at_time).order_by('-arrived_time').first()
        sor_of_direction = self.bus_route.stoponroute_set.filter(
                    direction=self.direction)
        sor = sor_of_direction.get(bus_stop=sob.bus_stop)
        sors = sor_of_direction.filter(order__range=(sor.order+1, self.order))
        sum_minute = sors.aggregate(models.Sum('average_time'))
        # est_time = datetime.combine(timezone.now(), sob.arrived_time
        #         ) + timedelta(minutes=sum_minute['average_time__sum'])
        est_time = calc_eta(sob.arrived_time, sum_minute['average_time__sum'])
        # return est_time.astimezone(pytz.timezone(settings.TIME_ZONE))
        return est_time

    def get_stop(self, offset=0, first=False, last=False):
        '''
        Return of stops on routes those have order offset from self.
        Example: four stops on route S1, S2, S3, S4
            S1.get_stops(3) = S4
            S3.get_stops(-1) = S2
            S2.get_stops(first=True) = S1
            S2.get_stops(last=True) = S4
        '''
        sor_of_direction = self.bus_route.stoponroute_set.filter(
                    direction=self.direction)
        if first or last:
            stoponroute = sor_of_direction.order_by(('' if first else '-')
                    + 'order').first()
        else:
            try:
                stoponroute = sor_of_direction.get(order=self.order+offset)
            except (StopOnRoute.DoesNotExist): stoponroute = None
        return stoponroute

    #def get_offset(self, stop_on_route):
    #    '''
    #    Return number of stops from self to the stop_on_route.
    #    Example: four stops on route S1, S2, S3, S4
    #        S1.get_offset(S4) = 3
    #        S3.get_offset(S2) = -1
    #    '''


class Bus(ExternalIdModel):
    bus_route = models.ForeignKey(BusRoute, on_delete=models.CASCADE)
    bus_stop = models.ManyToManyField(BusStop, verbose_name='Bus stop',
            through='StopOfBus', through_fields=('bus', 'bus_stop'))
    start_time = models.TimeField('Start time')
    next_bus_stop = models.ForeignKey(StopOnRoute,
            verbose_name='Next bus stop', on_delete=models.CASCADE)
    direction = models.CharField('Direction', max_length=20)

    class Meta:
        verbose_name = 'Bus'
        verbose_name_plural = 'Buses'

    # def get_sor_not_start_route(self, bus_stops, list_sors):
    #     sobs = StopOfBus.objects.filter(bus=self)
    #     stops = bus_stops.filter(stopofbus__in=sobs)
    #     sors = list_sors.exclude(bus_stop__in=stops)
    #     return sors

    @classmethod
    def get_buses_not_start(klass, at_time, sum_minute, sor, num_buses):
        arrived_sobs = StopOfBus.objects.filter(arrived_time__lt=at_time,
                bus__bus_route=sor.bus_route, bus__direction=sor.direction)
        buses_not_start = klass.objects.exclude(stopofbus__in=arrived_sobs
                ).filter(direction=sor.direction, bus_route=sor.bus_route)
        #print('count buses', buses_not_start.count())
        result = []
        count = 0
        for bus in buses_not_start:
            # eta = (datetime.combine(timezone.now(), bus.start_time)
            #     + timedelta(minutes=sum_minute))
            eta = calc_eta(bus.start_time, sum_minute)
            # eta = eta.astimezone(pytz.timezone(settings.TIME_ZONE))
            if eta >= at_time: pass
            else:
                continue # pragma: no cover
            result.append((bus, eta))
            count += 1
            if count >= num_buses: break
        #print('get_buses_not_start', result)
        return result

    def describe(self): # pragma: no cover
        return '{0} {1}'.format(self.external_id, self.bus_route.name)


class StopOfBus(models.Model):
    ''' Many to many relation between bus tops and buses. '''
    bus_stop = models.ForeignKey(BusStop, verbose_name='Bus stop',
            on_delete=models.CASCADE)
    bus = models.ForeignKey(Bus, verbose_name='Bus', on_delete=models.CASCADE)
    arrived_time = models.TimeField(_('Arrived time'), blank=True, null=True)

    class Meta:
        verbose_name = 'Stop of Bus'
        verbose_name_plural = 'Stops of Bus'


#class ForwardManager(models.Manager):
#    def get_queryset(self):
#        '''
#        return super().get_queryset().filter(direction='forward')
#        '''


#class BackwardManager(models.Manager):
#    def get_queryset(self):
#        '''
#        return super().get_queryset().filter(direction='backward')
#        '''
