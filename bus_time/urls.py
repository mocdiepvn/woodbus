from django.urls import path
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

from . import views as btv
from main import views as main_views

app_name = "bus_time"

urlpatterns = [
    path('search/', btv.SearchResult.as_view(), name='search'),
    path('search_input/', btv.SearchInput.as_view(), name='search_input'),
    path('bus_stops/<slug:ext_id>', btv.bus_stop_detail, name='bus_stops'),
    path('timezone/', btv.set_time, name='set_tz'),
    # path('set_timezone/', btv.set_timezone, name='set_timezone'),
    path('streets/<slug:street_slug>/', btv.street_detail, name='street_detail'),
    path('routes/<slug:id_route>/', btv.route_detail, name='route_detail'),
    # path('timezone/', btv.set_timezone, name='timezone'),
    path('', main_views.index, name='index'),
]
