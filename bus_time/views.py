import itertools
import pytz

from datetime import datetime, timedelta  # datetime: class
# from django.core.cache import cache
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.utils import timezone
from django.views import View
# from django.views.generic.detail import DetailView

from . import models as bm
from .middlewares import TimezoneMiddleware
from main.views import auto_load_tmpl, index

DT_FORMAT = '%Y-%m-%d %H:%M %Z'
test_data = {
    'test_time': None
}


class SearchResult(View):
    def get(self, request):
        search = request.GET.get('q')
        # print('search', search)
        found_items = bm.search(search.strip())
        template = 'bus_time/search_result.html'
        return render(request, template, context=found_items)


class SearchInput(View):
    def get(self, request):
        # print(request.GET)
        search = request.GET.get('q') or ''
        context = {
            'search_text': search,
            'timezones': pytz.common_timezones,
            # 'street_slug': 'da-nang-ong-ich-khiem',
            'at_time': request.session.get('at_time') or ''
        }
        if request.GET.get('ic-request'):
            template = 'bus_time/search_input.html'
        else:
            return index(request)
        # print('SearchInput', context)
        return render(request, template, context)


def get_at_time(request):
    '''
    Returns time in %Y-%m-%d %H:%M %Z format from one of below sources with
    that priority.
        test_data
        request.GET
        timezone.now()
    '''
    tz = pytz.timezone(request.session.get('django_timezone')
            or settings.TIME_ZONE)
    test_time = test_data['test_time']  # %H:%M
    at_time = test_time or request.session.get('at_time')  # %H:%M
    now = timezone.now()  # to have date
    now = now.astimezone(tz)
    at_time = datetime.strptime(at_time, '%H:%M') if at_time else now
    result = datetime.combine(now, at_time.time(), now.tzinfo)
    # print('at_time_view', result)
    return result


def bus_stop_detail(request, ext_id):
    bus_stop = get_object_or_404(bm.BusStop, external_id=ext_id)  # A.1.
    context = {
        'bus_stop': bus_stop,
        'route_sor': list(zip(bus_stop.busroute_set.all(),
                              bus_stop.stoponroute_set.all())),
        'bus_estm': bus_stop.get_routes_with_buses_and_eta(
            get_at_time(request), 5),
    }
    if request.GET.get('ic-request') == 'true':
        template = 'bus_time/bus_stop.html'
        # print('ic-request', request.GET.get('ic-request'))
    else:
        # context['auto_load'] = auto_load_tmpl.format(request.get_full_path())
        return index(request,
                     auto_load=auto_load_tmpl.format(request.get_full_path()))
    return render(request, template, context)


def set_time(request):
    at_time = request.GET.get('at_time') or request.session.get('at_time')
    request.session['at_time'] = at_time
    # get timezone
    timezone_name = request.GET.get('timezone') or 'No timezone'
    if TimezoneMiddleware.set(timezone_name) and timezone_name != "":
        request.session['django_timezone'] = timezone_name
    else: pass

    try:
        name, obj_id = request.GET.get('object_with_id').split('-', 1)
        if name == 'bus_route':
            return route_detail(request, obj_id)
        else:
            return street_detail(request, obj_id)
    except AttributeError:
        return index(request)


def street_detail(request, street_slug):
    street = get_object_or_404(bm.Street, slug=street_slug)
    even_stops = street.get_bus_stops(True)
    odd_stops = street.get_bus_stops(False)
    # TODO Show direction of routes
    even_sors = bm.StopOnRoute.objects.filter(bus_stop__in=even_stops)
    odd_sors = bm.StopOnRoute.objects.filter(bus_stop__in=odd_stops)
    sors_prev_street = street.get_sors_on_other_street(even_sors, -1)
    sors_prev_street_odd = street.get_sors_on_other_street(odd_sors, 1)
    pre_street_odd = [sor.bus_stop.street for sor in sors_prev_street_odd]
    pre_street_even = [sor.bus_stop.street for sor in sors_prev_street]

    sor_first_street = street.get_sors_on_other_street(
        sors_prev_street, first=True)
    sor_first_street_rt = street.get_sors_on_other_street(
        sors_prev_street_odd, first=True)
    # print('first_view',[sor.bus_stop.street for sor in sor_first_street])

    sors_next_street = street.get_sors_on_other_street(even_sors, 1)
    sors_next_street_odd = street.get_sors_on_other_street(odd_sors, -1)
    next_street_odd = [sor.bus_stop.street for sor in sors_next_street_odd]
    next_street_odd.extend(pre_street_odd)
    next_street_even = [sor.bus_stop.street for sor in sors_next_street]
    next_street_even.extend(pre_street_even)
    # print('street_odd', next_street_odd)
    check_next = set(next_street_odd) == set(next_street_even)
    # print('check', check_next)

    sor_last_street = street.get_sors_on_other_street(
        sors_next_street, last=True)
    sor_last_street_rt = street.get_sors_on_other_street(
        sors_next_street_odd, last=True)

    # print('sor_need_next', sors_next_street)

    def func_end_div(sors):
        end_div = []
        for i, sor in enumerate(sors):
            # try:
            #    if sor.bus_route == sor[i+1].bus_route:
            #        end_div.append(False)
            #        continue
            #    else: pass
            # except (IndexError): pass
            # end_div.append(True)
            try:
                sor_next = sors[i + 1].bus_route
            except (IndexError):
                sor_next = None
            # if sor.bus_route == sor_next:
            #    end_div.append(False)
            # else:
            #    end_div.append(True)
            end_div.append(sor.bus_route != sor_next)
        return end_div

    # TODO Previous end div list may differ than next end div list
    end_div = func_end_div(sors_prev_street or sors_next_street)
    # print('street_end_div', end_div)
    end_div_odd = func_end_div(sors_prev_street_odd or sors_next_street_odd)

    # buses_eta_even = []
    # print('div_end', end_div, '\n', sors_next_street, '\n', sor_last_street)
    at_time = get_at_time(request)
    buses_eta_even = [stop.get_routes_with_buses_and_eta(at_time, 2)
            for stop in even_stops]
    buses_eta_odd = [stop.get_routes_with_buses_and_eta(at_time, 2)
            for stop in odd_stops]

    context = {
        'street': street,
        'even_stops': list(zip(even_stops, buses_eta_even)),
        'odd_stops': list(zip(odd_stops, buses_eta_odd)),
        'previous_sor': list(zip(sors_prev_street, end_div,
                                 sor_first_street)),
        'previous_sor_odd': list(zip(sors_prev_street_odd,
                                     end_div_odd, sor_first_street_rt)),
        'next_sor': list(zip(sors_next_street, end_div, sor_last_street)),
        'next_sor_odd': list(zip(sors_next_street_odd, end_div_odd,
                                 sor_last_street_rt)),
        'check_next': check_next,
        'at_time': timezone.now(),
    }

    if request.GET.get('ic-request') == 'true':
        template = 'bus_time/street.html'
        # print('ic-request', request.GET.get('ic-request'))
    else:
        # context['auto_load'] = auto_load_tmpl.format(request.get_full_path())
        return index(request,
                     auto_load=auto_load_tmpl.format(request.get_full_path()))
        # print('this_is 2',request.get_full_path())

    # timezone.activate(pytz.timezone('Asia/Ho_Chi_Minh'))
    return render(request, template, context)


def route_detail(request, id_route):
    at_time = get_at_time(request)

    bus_route = get_object_or_404(bm.BusRoute, external_id=id_route)
    forward_sors = bus_route.stoponroute_set.filter(direction=bm.FORWARD)
    return_sors = bus_route.stoponroute_set.filter(direction=bm.RETURN)
    forward_buses = bus_route.bus_set.filter(direction=bm.FORWARD)
    return_buses = bus_route.bus_set.filter(direction=bm.RETURN)

    # Option 1: https://docs.djangoproject.com/en/3.1/ref/models/expressions/#subquery-expressions
    # Option 2: https://docs.djangoproject.com/en/3.1/ref/models/expressions/#raw-sql-expressions
    def save_count(sors):
        save_count = []
        count = 1
        for i, sor in enumerate(sors):
            try:
                next_street = sors[i + 1].bus_stop.street
            except (IndexError):
                next_street = None
            if next_street == sor.bus_stop.street:
                count += 1
            else:
                save_count.extend([count] * count)
                count = 1
        return save_count

    count_forward_stops_of_streets = save_count(forward_sors)
    count_return_stops_of_streets = save_count(return_sors)
    forward_sobs = bm.StopOfBus.objects.filter(arrived_time__lt=at_time,
            bus__bus_route=bus_route, bus__direction=bm.FORWARD)
    return_sobs = bm.StopOfBus.objects.filter(arrived_time__lt=at_time,
            bus__bus_route=bus_route, bus__direction=bm.RETURN)

    context = {
        'bus_route': bus_route,
        'forward_sors': list(zip(forward_sors,
                                 count_forward_stops_of_streets)),
        'return_sors': list(zip(return_sors,
                                count_return_stops_of_streets)),
        'forward_buses': forward_buses,
        'return_buses': return_buses,
        'forward_sobs': forward_sobs,
        'return_sobs': return_sobs,
        'forward_eta_list': bus_route.calculate_eta_table(at_time,
            forward_sors, forward_sobs, bm.FORWARD, forward_buses),
        'return_eta_list': bus_route.calculate_eta_table(at_time,
            return_sors, return_sobs, bm.RETURN, return_buses)
    }

    if request.GET.get('ic-request') == 'true': # pragma: no cover
        template = 'bus_time/bus_route.html'
        # print('ic-request', request.GET.get('ic-request'))
    else:
        # context['auto_load'] = auto_load_tmpl.format(request.get_full_path())
        return index(request,
                     auto_load=auto_load_tmpl.format(request.get_full_path())) # pragma: no cover
        # print('ic', request.GET.get('ic-request'))
    return render(request, template, context)
