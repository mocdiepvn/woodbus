from django.contrib import admin
from . import models as btmd


class StopOnRouteInline(admin.TabularInline):
    model = btmd.StopOnRoute


class StopOfBusInline(admin.TabularInline):
    model = btmd.StopOfBus


class BusAdmin(admin.ModelAdmin):
    model = btmd.Bus
    inlines = [StopOfBusInline]


class BusRouteAdmin(admin.ModelAdmin):
    inlines = [StopOnRouteInline]


admin.site.register(btmd.BusRoute, BusRouteAdmin)
# 1. Add inline table to BusRoute to show StopOnRoute
#     Order by direction, order
admin.site.register(btmd.Bus, BusAdmin)
admin.site.register(btmd.Street)
admin.site.register(btmd.BusCompany)
admin.site.register(btmd.BusStop)
admin.site.register(btmd.StopOnRoute)
admin.site.register(btmd.StopOfBus)

