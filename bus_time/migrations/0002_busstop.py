# Generated by Django 2.2 on 2020-07-17 09:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bus_time', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='bus',
            options={'verbose_name': 'Bus', 'verbose_name_plural': 'Buses'},
        ),
        migrations.AddField(
            model_name='busstop',
            name='latitude',
            field=models.FloatField(default=0, verbose_name='Latitude'),
        ),
        migrations.AddField(
            model_name='busstop',
            name='longitute',
            field=models.FloatField(default=0, verbose_name='Longitute'),
        ),
        migrations.AddField(
            model_name='busstop',
            name='order',
            field=models.IntegerField(blank=True, null=True, verbose_name='Order'),
        ),
    ]
