from django.apps import AppConfig


class BusTimeConfig(AppConfig):
    name = 'bus_time'
