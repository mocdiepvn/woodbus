import pytz

from django.conf import settings
from django.utils import timezone

class TimezoneMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # print('middle_list_session', request.session.get('django_timezone'))
        tzname = request.session.get('django_timezone')

        TimezoneMiddleware.set(tzname)
        return self.get_response(request)

    @classmethod
    def set(klass, tzname):
        if tzname:
            try:
                # print('activate', tzname)
                timezone.activate(pytz.timezone(tzname))
            except (pytz.exceptions.UnknownTimeZoneError):
                # print('key error', settings.TIME_ZONE)
                # timezone.activate(pytz.timezone(settings.TIME_ZONE))
                return False
        else:
            # timezone.deactivate()
            timezone.activate(pytz.timezone(settings.TIME_ZONE))
            # print('deactivate', tzname)
        return True
