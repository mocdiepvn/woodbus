behave-django==1.3.0
coverage==5.0.3
django-coverage-plugin==1.6.0
splinter==0.13.0
